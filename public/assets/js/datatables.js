/* ============================================================
 * DataTables
 * Generate advanced tables with sorting, export options using
 * jQuery DataTables plugin
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */


(function($) {

    'use strict';

    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    // Initialize datatable showing a search box at the top right corner
    var initTableWithSearch = function() {
        var table = $('#tableWithSearch');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    // Initialize datatable with ability to add rows dynamically
    var initTableWithDynamicRows = function() {
        var table = $('#tableWithDynamicRows');


        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };


        table.dataTable(settings);

        $('#show-modal').click(function() {
            $('#addNewAppModal').modal('show');
        });

        $('#add-app').click(function() {
            table.dataTable().fnAddData([
                $("#appName").val(),
                $("#appDescription").val(),
                $("#appPrice").val(),
                $("#appNotes").val()
            ]);
            $('#addNewAppModal').modal('hide');

        });
    }

    // Initialize datatable showing export options
    var initTableWithExportOptions = function() {

        var table = $('#tableWithExportOptions');

        var settings = {
            "sDom": "<'exportOptions'T><'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "oTableTools": {
                "sSwfPath": "assets/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "csv",
                    "sButtonText": "<i class='pg-grid'></i>",
                }, {
                    "sExtends": "xls",
                    "sButtonText": "<i class='fa fa-file-excel-o'></i>",
                }, {
                    "sExtends": "pdf",
                    "sButtonText": "<i class='fa fa-file-pdf-o'></i>",
                }, {
                    "sExtends": "copy",
                    "sButtonText": "<i class='fa fa-copy'></i>",
                }]
            },
            fnDrawCallback: function(oSettings) {
                $('.export-options-container').append($('.exportOptions'));

                $('#ToolTables_tableWithExportOptions_0').tooltip({
                    title: 'Export as CSV',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_1').tooltip({
                    title: 'Export as Excel',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_2').tooltip({
                    title: 'Export as PDF',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_3').tooltip({
                    title: 'Copy data',
                    container: 'body'
                });
            },
            oLanguage : {
                "sProcessing" : "처리중...",
                "sZeroRecords" : "데이터가 없습니다.",
                "sLengthMenu" : "_MENU_ 건 표시",
                "oPaginate" : {
                  "sFirst" : "처음",
                  "sNext" : "다음",
                  "sPrevious" : "이전",
                  "sLast" : "끝"
                },
                "sInfo" : "총 _TOTAL_ 건 중, _START_ 건부터 _END_ 건까지 표시",
                "sInfoEmpty" : "0건 중, 0 부터 0까지 표시",
                'sInfoFilterd' : "(총 _MAX_ 건에서 추출)",
                "sSearch" : "검색 : "
              }, 
              processing : true,
              bPaginate : true,  	// 페이징
              iDisplayLength : 10,	// 기본 표시행
              bLengthChange : true,	// 보여주는 건수 선택
              bFilter : true,		// 검색
              bSort : true,			// 소트
              bAutoWidth : true,			
            
            //   ajax : 
            //   {
            //     // url : '/studenttestpapers'
            //     url : '/studenttestpapers'
            //   },
              
              columns: [			
                { data: "username" },
                { data: "name" },
                { 
                    data: "startdate",
                    render: function ( data, type, row ) 
                    {
                        // If display or filter data is requested, format the date
                        if ( type === 'display' || type === 'filter' ) 
                        {
                            var rowvalue = row["EventDate"];
                            var rowvalueallday = row["AllDayEvent"];
                            return (moment(data).format("YYYY-MM-DD"));
                        }
                    return data;
                    }
                },
                { 
                    data: "enddate",
                    render: function ( data, type, row ) 
                    {
                        // If display or filter data is requested, format the date
                        if ( type === 'display' || type === 'filter' ) 
                        {
                            var rowvalue = row["EventDate"];
                            var rowvalueallday = row["AllDayEvent"];
                            return (moment(data).format("YYYY-MM-DD"));
                        }
                    return data;
                    }
                },
                { 
                    data: "status",
                    render: function(data, type, row)
                    {
                        // return '<a style="cursor:pointer" onclick="doTest('+data+')">'+"정답입력"+'</a>';

                        let retValue = '';
                        console.log("data: status");
                        console.log(data);
                        return '<a onclick="doTest('+row.id+')">'+"정답입력"+'</a>';
                        // if(data === '미응시')
                        // {
                            // retValue = '<a onclick="doTest('+row.id+')">'+data+'</a>';
                    //     }
                    //     else
                    //     {
                    //         if ( type === 'display' || type === 'filter' ) 
                    //         {
                    //             var rowvalue = row["EventDate"];
                    //             var rowvalueallday = row["AllDayEvent"];
                    //             return (moment(data).format("YYYY-MM-DD"));
                    //         }
                    //         retValue = '<a>'+(moment(data).local().format("YYYY-MM-DD"))+'</a>';
                    //     }
                    //   return retValue;
                    }
                },
                { 
                  data: "path",
                  render: function(data, type, row)
                  {
                    return '<a style="cursor:pointer" href="' + getFileDown(data) + '" download="">다운로드</a>';
                  }
                },
              ],
              order : [[0, 'desc']]
        };

        // var url2 = '/studenttestpapers/' + $('#userid').val();
        // console.log(url2);
        // $('#datable_1').DataTable().table.ajax.url(url2).load(null, false);

        table.dataTable(settings);

    }

    initTableWithSearch();
    initTableWithDynamicRows();
    initTableWithExportOptions();

})(window.jQuery);