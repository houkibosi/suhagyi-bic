/*FormXeditable Init*/
$(function(){
	"use strict";
	
	/*This week*/
	$('#thisweekreport-mon').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#thisweekreport-tue').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#thisweekreport-wed').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#thisweekreport-thu').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#thisweekreport-fri').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#thisweekreport-etc').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	/*Next week*/
	$('#nextweek-mon').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#nextweek-tue').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#nextweek-wed').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#nextweek-thu').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#nextweek-fri').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});

	$('#nextweek-etc').editable({
	  showbuttons: 'bottom',
	  mode: 'inline'
	});
});