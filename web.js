// default
const express = require('express');
const expressSession = require('express-session');
const fs = require('fs');
const ejs = require('ejs');
const passport = require('passport');
const passportLocal = require('passport-local');
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash');
const router = require('./controllers/index');
const cookieSession = require('cookie-session');
const cookieParser = require('cookie-parser');
const models = require('./models/models');
const path = require('path');

// import module
const bodyParser = require('body-parser');
const app = express();


// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// 위치 초기화
app.use(express.static(__dirname + '/public'));

// View
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);



// passport 설정
app.use(cookieParser());
app.use(expressSession({
  key: 'sid',
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 //1시간
  }
}));

// passport 초기화
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// app.use(cookieSession({
//   keys: ['cookie'],
//   cookie: {
//       maxAge: 1000 * 60 * 60 // 1시간
//   }
// }));

// module.exports = function(passport){
//   passport.serializeUser(function(user, done){
//     done(null, user.id);
//   });
//   passport.deserializeUser(function(id, done){
//     User.findById(id, function(err, user){
//       done(err, user);
//     });
//   });
// };

// router
app.use(router);

// 폴더 확인
function getFiles (dir, files_){
  files_ = files_ || [];
  var files = fs.readdirSync(dir);
  for (var i in files){
    var name = dir + '/' + files[i];
    if (fs.statSync(name).isDirectory()){
        getFiles(name, files_);
    } else {
        files_.push(name);
    }
  }
  return files_;
}

console.log(getFiles(__dirname+'/public/uploads'));


// server
app.listen(8001, () => {
  console.log('Example app listening!');

  models.sequelize.sync({ force: false })
    // 관리자 계정 생성
    .then(() => models.User.findOne(
      {
        where: {
          userid: 'admin'
        }
      }
    ).then(user => 
      {
        if (user == null) 
        {
          models.User.create(
            {
            companyid: '0',
            team: '',
            userid: 'admin',
            name: '관리자',
            email: '',
            phone: '',
            password: 'password',
            isadmin: 'true'
          })
        }
      }
    )
  ).then(() => {
    try{ 
      fs.mkdirSync(path.join("public", "uploads", "1"));
    } catch(e) { 
      if ( e.code != 'EEXIST' ) {
        console.log(e);
      }
    }

    try{ 
      fs.mkdirSync(path.join("public", "uploads", "2"));
    } catch(e) { 
      if ( e.code != 'EEXIST' ) {
          console.log(e);
      }
    }
    
    try{ 
      fs.mkdirSync(path.join("public", "uploads", "3"));
    } catch(e) { 
      if ( e.code != 'EEXIST' ) {
          console.log(e);
      }
    }
    
    try{ 
      fs.mkdirSync(path.join("public", "uploads", "4"));
    } catch(e) { 
      if ( e.code != 'EEXIST' ) {
          console.log(e);
      }
    }
    
    try{ 
      fs.mkdirSync(path.join("public", "uploads", "5"));
    } catch(e) { 
      if ( e.code != 'EEXIST' ){
          console.log(e);
      }
    }
    
    try{ 
      fs.mkdirSync(path.join("public", "uploads", "6"));
    } catch(e) { 
      if ( e.code != 'EEXIST' ) {
          console.log(e);
      }
    }
  }
)
    
});
