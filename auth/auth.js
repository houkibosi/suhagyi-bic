const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const LocalStrategy = require('passport-local').Strategy;
const cookieSession = require('cookie-session');
const flash = require('connect-flash');
const expressSession =  require('express-session');

const models = require('../models/models');




//module.exports = () => {
  passport.serializeUser((user, done) => { // Strategy 성공 시 호출됨
    console.log("serializeUser()!!");
    //console.log("로그인 성공 ???");

    //console.log("user -> ", user);

    //done(null, user.dataValues.userid); // 여기의 user._id가 req.session.passport.user에 저장
    //done(null, user);
    done(null, user);
  });

  passport.deserializeUser((user, done) => { // 매개변수 id는 req.session.passport.user에 저장된 값
    console.log("deserializeUser()!!");
    console.log(user);
    //models.User.findById(user, (err, user) => {
      return done(null, user); // 여기의 user가 req.user가 됨
    //});
  });


//}

// passport.use('basic', new BasicStrategy(
//     function (username, password, callback)
//     {
//         models.User.findOne(
//         {
//             usernameField : 'username',
//             passwordField : 'password',
//             passReqCallback : true
//         },
//         function (err, user)
//         {
//             if(err) {return callback('실패')}
//             if(!user) {
//                 return callback(null, false, { message: '유저가 없습니다.'});
//             }
//             if(!user.validPassword(password)) {
//                 return callback(null, false, {message: '비밀번호가 틀렸습니다.'});
//             }
//             return callback(null, user);

//             // if(username===user.userid && password===user.password)
//             // {
//             //     callback(null, username);
//             // }
//             // else
//             // {
//             //     callback(null, false);
//             // }
//         });
//     }
// ));

passport.use('local-login', new LocalStrategy({
  session: true,
  passReqToCallback: true
},
  function(req, username, password, done) {

    const models = require('../models/models');

    models.User.findOne({
      where: {
        userid: username,
        password: password
      }
    }).then((user) => {

      models.loginhistory.create({
        userid : username,
        ip : req.connection.remoteAddress
      })

      return done(null, user);

    },
    (error)=> {

      console.log("Error -> ", error);

    }).catch((exp) => {

      console.log("예외 발생 !!");
      console.log(exp);

    })
  })
);

// passport.use(new LocalStrategy({
//     usernameField: 'userid',
//     passwordField: 'password',
//     passReqToCallback: true
// },
//     function (req, username, password, callback) {
//         models.User.findOne({'userid': username}, function (err, user) {
//             if(err) {return callback(err)}
//             if(!user) {
//                 return callback(null, false, { message: 'incorrect username.'});
//             }
//             if(!user.validPassword(password)) {
//                 return callback(null, false, {message: 'Incorrect password.'});
//             }
//             return callback(null, user);
//         });
//     }
// ))

//app.use(passport.session());

//exports.isBasicAuthenticated = passport.authenticate('basic', {session: true});
exports.isBasicAuthenticated = passport.authenticate('local-login', {session: true});
