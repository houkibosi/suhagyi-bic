const models = require('../models/models');

/* 태이블 정보
    writer: sequelize.INTEGER,
    status: Sequelize.INTEGER,
    memo: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.attiBoard.findAll()
        .then(attiboard => res.json(attiboard));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.attiBoard.findOne({
      where: {
        id: id
      }
    }).then(attiboard => {
      if (!attiboard) {
        return res.status(404).json({error: 'No attiboard'});
      }
  
      return res.json(attiboard);
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.attiBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const writer= req.body.writer || '';
  const status= req.body.status || '';
  const memo= req.body.memo || '';

    models.attiBoard.create({
      writer: writer,
      status: status,
      memo: memo
    }).then((attiboard) => res.status(201).json(attiboard))
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  