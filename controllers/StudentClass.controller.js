const models = require('../models/models');

/* 태이블 정보
    name: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.StudentClass.findAll()
        .then(studentClass => res.json(studentClass));
};
  
// 학년별 찾기 찾기
exports.show = (req, res) => {
    const grade = parseInt(req.params.grade, 10);
    console.log(grade);
    
    if (!grade) {
      return res.status(400).json({error: 'Incorrect id'});
    }

    console.log(grade);
  
    models.StudentClass.findAll({
      where: {
        grade: grade
      }
    }).then(studentClass => {
      if (!studentClass) {
        return res.status(404).json({error: 'No studentClass'});
      }
  
      return res.status(200).json(studentClass);
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.body.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.StudentClass.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const grade = req.body.grade || '';
    const name = req.body.name || '';
    const explanation = req.body.explanation || '';
    if (!name.length) {
      return res.status(400).json({error: 'Incorrenct name'});
    }
  
    models.StudentClass.create({
      grade: grade,
      name: name,
      explanation: explanation
    }).then((studentClass) => res.status(201).json(studentClass))
};

// 업데이트
exports.update = (req, res) => {
  const id = req.body.id;
  const name = req.body.name;

  console.log("update id = " + id + ', name = ' + name);

  models.StudentClass.update(
    {
      name: name
    }, 
    {
      where: 
      {
        id:id
      }
    })
    .then((data) => res.status(200).json(data))
  }

  