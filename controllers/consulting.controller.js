const models = require('../models/models');
const moment = require('moment');

/* 태이블 정보
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    writer: Sequelize.INTEGER
*/

// 전체 찾기
exports.index = (req, res) => {
  models.consulting.findAll({
    order: [['createdAt', 'DESC']]
  },
).then(data => res.status(200).json(
    {
      'data' : data
    }
  ));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const userid = req.params.userid;
    if (!userid) {
      return res.status(400).json({error: 'Incorrect id'});
    }

    var strQuery = '';
    strQuery += 'select cons.*, consre.content as replycontent, consre.writer as replywriter  ';
    strQuery += 'from consultings cons ';
    strQuery += 'left outer join consultingreplies consre on cons.id = consre.boardid ';
    strQuery += 'where cons.writer = "'+userid+'" ';

    models.sequelize.query(strQuery, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.status(200).json(
        {
          'data' : data
        }
      ));
  
    // models.consulting.findAll({
    //   where: {
    //     writer: userid
    //   }
    // }).then(data => {
    //   if (!data) {
    //     return res.status(404).json({error: 'No consulting'});
    //   }
  
    //   return res.status(200).json(
    //   {
    //     'data' : data
    //   });
    // });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.consulting.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const title = req.body.title || '';
    const content = req.body.content || '';
    const writer = req.body.writer || '';
    const writername = req.body.writername || '';

    models.consulting.create({
      title: title,
      content: content,
      writer: writer,
      writername: writername
    }).then(() => res.status(200).json())
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  