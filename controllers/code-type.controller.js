const models = require('../models/models');

/* 태이블 정보
    writer: sequelize.INTEGER,
    status: Sequelize.INTEGER,
    memo: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
  const nGrade = parseInt(req.params.grade, 10);

  models.typeCode.findAll({
    where: {
      nGrade: nGrade
    }
  }).then(data => res.json({
    'data' : data
  }));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.typeCode.findOne({
      where: {
        id: id
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No typeCode'});
      }
  
      return res.json({
        'data' : data
      });
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.typeCode.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const nGrade= req.body.nGrade || '';
  const sCode= req.body.sCode || '';
  const sValue= req.body.sValue || '';

    models.typeCode.create({
      nGrade: nGrade,
      sCode: sCode,
      sValue: sValue
    }).then((data) => res.status(201).json(data))
};

// 업데이트
exports.update = (req, res) => {
  const body = req.body;

  const id = parseInt(req.body.id, 10);
  const sCode = req.body.sCode;
  const sValue = req.body.sValue;

  console.log(id);
  console.log(sCode);
  console.log(sValue);

  models.typeCode.update(
    {
      sCode: sCode,
      sValue: sValue
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((data) => res.status(200).json(data))
  }

  