const models = require('../models/models');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    Studentsid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.User.findAll({
      where: {
        isstudent: true          
      }
    }).then(data => res.json(
      {
        'data' : data
      }
    ));
};
  
// 클레스 기준 찾기
exports.show = (req, res) => {
    const grade = parseInt(req.params.grade, 10);
    const classname = parseInt(req.params.classname, 10);
    if (!grade) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.User.findAll({
      where: {
        grade: grade,
        classname: classname,
        isstudent: '1'
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Students'});
      }
  
      return res.json(
        {
          'data' : data
        }
      );
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.User.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const grade = req.body.grade || '';
    const classname = req.body.classname || '';
    const userid = req.body.userid || '';
    const name = req.body.name || '';
    const password = req.body.password || '';
    const isstudent = req.body.isstudent || 0;
    const isparent = req.body.isparent || 0;
  
    models.User.create({
      grade: grade,
      classname: classname,
      userid: userid,
      name: name,
      password: password,
      isstudent: isstudent,
      isparent: isparent
    }).then((Students) => res.status(201).json(Students))
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  