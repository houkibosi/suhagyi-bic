const models = require('../models/models');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const PDF = require('pdfkit');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    Studentsid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.testpaper.findAll({
    })
    // models.sequelize.query("SELECT distinct a.name as name, a.startdate, a.enddate, a.writer, a.path FROM testpapers a;")
    .then(data => res.json(
      {
        'data' : data
      }
    ));
};
  
// 클레스 기준 찾기
exports.show = (req, res) => {
    const grade = parseInt(req.params.grade, 10);
    const userid = req.params.userid;
    if (!grade) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.testpaper.findAll({
      where: {
        grade: grade,
        writer: userid
      }
    })
    // models.sequelize.query('SELECT distinct a.name as name, a.startdate, a.enddate, a.writer, a.path FROM testpapers a where grade='+grade+' and writer = "'+userid+'";')
    .then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Students'});
      }
  
      return res.json(
        {
          'data' : data
        }
      );;
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.body.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.testpaper.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
      const name = req.body.name || '';
      const startdate = req.body.startdate || '0000-00-00';
      const enddate = req.body.enddate || '0000-00-00';
      const grade = req.body.grade || '';
      const classIndex = req.body.classIndex || '';
      const writer = req.body.writer || '';
      const total = req.body.total || 0;
      const question1 = parseInt(req.body.question1, 10) || null;
      const question2 = parseInt(req.body.question2, 10) || null;
      const question3 = parseInt(req.body.question3, 10) || null;
      const question4 = parseInt(req.body.question4, 10) || null;
      const question5 = parseInt(req.body.question5, 10) || null;
      const question6 = parseInt(req.body.question6, 10) || null;
      const question7 = parseInt(req.body.question7, 10) || null;
      const question8 = parseInt(req.body.question8, 10) || null;
      const question9 = parseInt(req.body.question9, 10) || null;
      const question10 = parseInt(req.body.question10, 10) || null;
      const question11 = parseInt(req.body.question11, 10) || null;
      const question12 = parseInt(req.body.question12, 10) || null;
      const question13 = parseInt(req.body.question13, 10) || null;
      const question14 = parseInt(req.body.question14, 10) || null;
      const question15 = parseInt(req.body.question15, 10) || null;
      const question16 = parseInt(req.body.question16, 10) || null;
      const question17 = parseInt(req.body.question17, 10) || null;
      const question18 = parseInt(req.body.question18, 10) || null;
      const question19 = parseInt(req.body.question19, 10) || null;
      const question20 = parseInt(req.body.question20, 10) || null;
      const question21 = parseInt(req.body.question21, 10) || null;
      const question22 = parseInt(req.body.question22, 10) || null;
      const question23 = parseInt(req.body.question23, 10) || null;
      const question24 = parseInt(req.body.question24, 10) || null;
      const question25 = parseInt(req.body.question25, 10) || null;
      const question26 = parseInt(req.body.question26, 10) || null;
      const question27 = parseInt(req.body.question27, 10) || null;
      const question28 = parseInt(req.body.question28, 10) || null;
      const question29 = parseInt(req.body.question29, 10) || null;
      const question30 = parseInt(req.body.question30, 10) || null;

      let q1path = '';
      const question1path = models.question.findOne({
        where: {
          id: question1
        }
      }).then(data => {
        if(!data)
          return '';
        q1path = path.join(__dirname, data.image, data.imagename);
      }) || '';
      
      let q2path = '';
      const question2path =  models.question.findOne({
        where: {
          id: question2
        }
      }).then(data => {
        if(!data)
          return '';
        q2path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q3path = '';
      const question3path =  models.question.findOne({
        where: {
          id: question3
        }
      }).then(data => {
        if(!data)
          return '';
        q3path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q4path = '';
      const question4path =  models.question.findOne({
        where: {
          id: question4
        }
      }).then(data => {
        if(!data)
          return '';
        q4path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q5path = '';
      const question5path =  models.question.findOne({
        where: {
          id: question5
        }
      }).then(data => {
        if(!data)
          return '';
        q5path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q6path = '';
      const question6path =  models.question.findOne({
        where: {
          id: question6
        }
      }).then(data => {
        if(!data)
          return '';
        q6path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q7path = '';
      const question7path =  models.question.findOne({
        where: {
          id: question7
        }
      }).then(data => {
        if(!data)
          return '';
        q7path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q8path = '';
      const question8path =  models.question.findOne({
        where: {
          id: question8
        }
      }).then(data => {
        if(!data)
          return '';
          q8path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q9path = '';
      const question9path =  models.question.findOne({
        where: {
          id: question9
        }
      }).then(data => {
        if(!data)
          return '';
          q9path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q10path = '';
      const question10path = models.question.findOne({
        where: {
          id: question10
        }
      }).then(data => {
        if(!data)
          return '';
        q10path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q11path = '';
      const question11path = models.question.findOne({
        where: {
          id: question11
        }
      }).then(data => {
        if(!data)
          return '';
        q11path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q12path = '';
      const question12path = models.question.findOne({
        where: {
          id: question12
        }
      }).then(data => {
        if(!data)
          return '';
        q12path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q13path = '';
      const question13path = models.question.findOne({
        where: {
          id: question13
        }
      }).then(data => {
        if(!data)
          return '';
        q13path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q14path = '';
      const question14path = models.question.findOne({
        where: {
          id: question14
        }
      }).then(data => {
        if(!data)
          return '';
        q14path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q15path = '';
      const question15path = models.question.findOne({
        where: {
          id: question15
        }
      }).then(data => {
        if(!data)
          return '';
        q15path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q16path = '';
      const question16path = models.question.findOne({
        where: {
          id: question16
        }
      }).then(data => {
        if(!data)
          return '';
        q16path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q17path = '';
      const question17path = models.question.findOne({
        where: {
          id: question17
        }
      }).then(data => {
        if(!data)
          return '';
        q17path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q18path = '';
      const question18path = models.question.findOne({
        where: {
          id: question18
        }
      }).then(data => {
        if(!data)
          return '';
        q18path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q19path = '';
      const question19path = models.question.findOne({
        where: {
          id: question19
        }
      }).then(data => {
        if(!data)
          return '';
        q19path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q20path = '';
      const question20path = models.question.findOne({
        where: {
          id: question20
        }
      }).then(data => {
        if(!data)
          return '';
        q20path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q21path = '';
      const question21path = models.question.findOne({
        where: {
          id: question21
        }
      }).then(data => {
        if(!data)
          return '';
        q21path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q22path = '';
      const question22path = models.question.findOne({
        where: {
          id: question22
        }
      }).then(data => {
        if(!data)
          return '';
        q22path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q23path = '';
      const question23path = models.question.findOne({
        where: {
          id: question23
        }
      }).then(data => {
        if(!data)
          return '';
        q23path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q24path = '';
      const question24path = models.question.findOne({
        where: {
          id: question24
        }
      }).then(data => {
        if(!data)
          return '';
        q24path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q25path = '';
      const question25path = models.question.findOne({
        where: {
          id: question25
        }
      }).then(data => {
        if(!data)
          return '';
        q25path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q26path = '';
      const question26path = models.question.findOne({
        where: {
          id: question26
        }
      }).then(data => {
        if(!data)
          return '';
        q26path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q27path = '';
      const question27path = models.question.findOne({
        where: {
          id: question27
        }
      }).then(data => {
        if(!data)
          return '';
        q27path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q28path = '';
      const question28path = models.question.findOne({
        where: {
          id: question28
        }
      }).then(data => {
        if(!data)
          return '';
        q28path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q29path = '';
      const question29path = models.question.findOne({
        where: {
          id: question29
        }
      }).then(data => {
        if(!data)
          return '';
        q29path = path.join(__dirname, data.image, data.imagename);
      }) || '';

      let q30path = '';
      const question30path = models.question.findOne({
        where: {
          id: question30
        }
      }).then(data => {
        if(!data)
          return '';
        q30path = path.join(__dirname, data.image, data.imagename);
      }) || '';
  
    models.testpaper.create({
      name : name,
      grade : grade,
      class: classIndex,
      startdate: startdate,
      enddate: enddate,
      path: path.join(__dirname, '..', 'public', 'uploads', grade, 'pdf', name+'.pdf'),
      writer: writer,
      total: total,
      question1 : question1,
      question2 : question2,
      question3 : question3,
      question4 : question4,
      question5 : question5,
      question6 : question6,
      question7 : question7,
      question8 : question8,
      question9 : question9,
      question10 : question10,
      question11 : question11,
      question12 : question12,
      question13 : question13,
      question14 : question14,
      question15 : question15,
      question16 : question16,
      question17 : question17,
      question18 : question18,
      question19 : question19,
      question20 : question20,
      question21 : question21,
      question22 : question22,
      question23 : question23,
      question24 : question24,
      question25 : question25,
      question26 : question26,
      question27 : question27,
      question28 : question28,
      question29 : question29,
      question30 : question30,
    })
    .then((data) => res.status(200).json(data))
    // .then((data) => {
    //   var strQuery = '';
    // strQuery += 'insert into testpapers( name, writer, tester, startdate, enddate, grade, path, total,  ';
    // strQuery += 'question1, question2, question3, question4, question5, question6,  ';
    // strQuery += 'question7, question8, question9, question10, question11, question12, ';
    // strQuery += 'question13, question14, question15, question16, question17, question18, ';
    // strQuery += 'question19, question20, question21, question22, question23, question24, ';
    // strQuery += 'question25, question26, question27, question28, question29, question30, ';
    // strQuery += 'createdAt, updatedAt) ';
    // strQuery += 'select tp.name, tp.writer, us2.userid, tp.startdate, tp.enddate, tp.grade, tp.path, tp.total,  ';
    // strQuery += 'tp.question1, tp.question2, tp.question3, tp.question4, tp.question5, tp.question6,  ';
    // strQuery += 'tp.question7, tp.question8, tp.question9, tp.question10, tp.question11, tp.question12, ';
    // strQuery += 'tp.question13, tp.question14, tp.question15, tp.question16, tp.question17, tp.question18, ';
    // strQuery += 'tp.question19, tp.question20, tp.question21, tp.question22, tp.question23, tp.question24, ';
    // strQuery += 'tp.question25, tp.question26, tp.question27, tp.question28, tp.question29, tp.question30, ';
    // strQuery += 'tp.createdAt, tp.updatedAt ';
    // strQuery += 'from testpapers tp, users us, users us2 ';
    // strQuery += 'where tp.writer = us.userid and us.classname = us2.classname and us2.isstudent = 1 and tp.writer = "'+writer+'" and tp.name = "'+name+'"; ';

    //   console.log(strQuery);
      
    // models.sequelize.query(strQuery, { type: models.sequelize.QueryTypes.SELECT})  

    // })
    .then((data) => {
      let doc = new PDF();

      try
      { 
        if(path.join(__dirname, '..', "public", "uploads", grade, 'pdf') !== '')
        {
            lastfile = path.join(__dirname, '..', "public", "uploads", grade, 'pdf');
            fs.mkdirSync(lastfile);
            console.log("Make folder = " + lastfile);
        }
        else
        console.log('Folder is empty.');
      }
      catch(e)
      { 
        if ( e.code != 'EEXIST' ) 
          throw e; // 존재할경우 패스처리함. 
      }

      console.log("Doc Pipe");
      doc.pipe(fs.createWriteStream(path.join(__dirname, '..', "public", "uploads", grade, 'pdf', name+'.pdf')));

      console.log("Doc Font");
      doc.font(path.join(__dirname, '..', 'public', 'font', 'Spoqa Han Sans Regular.ttf'));
      // path.exists(__dirname + '/../public/uploads/'+grade+'/pdf/'+name+'.pdf', function(exists){
      //   console.log(exists);
      // })


      // console.log('path = ' + path.existsSync(__dirname + '/../public/uploads/'+grade+'/pdf'));


      

      // 페이지 1
      // 제목
      var gradeText="";
      switch(grade){
        case "1" : (gradeText = "중1"); break;
        case "2" : (gradeText = "중2"); break;
        case "3" : (gradeText = "중3"); break;
        case "4" : (gradeText = "고1"); break;
        case "5" : (gradeText = "고2"); break;
        case "6" : (gradeText = "고3"); break;
      }
      doc.fontSize(12);
      doc.text(gradeText, 30, 35, {align : 'left'}); 
      doc.fontSize(30);
      doc.text(name, 80, 15, {align : 'center'}); 

      // 양식
      doc.lineCap('round')
        .moveTo(25, 60)
        .lineTo(580, 60)
        .stroke()
      doc.moveDown()

      doc.lineCap('round')
        .moveTo(300, 70)
        .lineTo(300, 770)
        .stroke()
      doc.moveDown()

      // 내용
      console.log('question1path = ' + q1path); 
      if(typeof(question1) != 'undefined' && question1 != null && q1path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 01', 25, 60, {align : 'left'});
          doc.image (q1path, 25, 80, {height : 200, width : 250}); 
        }

      console.log('question2path = ' + q2path);
      if(typeof(question2) != 'undefined' && question2 != null && q2path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 02', 25, 300, {align : 'left'});
          doc.image (q2path, 25, 320, {height : 200, width : 250}); 
        }

      console.log('question3path = ' + q3path);
      if(typeof(question3) != 'undefined' && question3 != null && q3path != '')
        {
          doc.fontSize(10);
          doc.text('문제 03', 25, 540, {align : 'left'}); 
          doc.image (q3path, 25, 560, {height : 200, width : 250}); 
        }

      console.log('question4path = ' + q4path);
      if(typeof(question4) != 'undefined' && question4 != null && q4path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 04', 325, 60, {align : 'left'});
          doc.image (q4path, 325, 80, {height : 200, width : 250}); 
        }

      console.log('question5path = ' + q5path);
      if(typeof(question5) != 'undefined' && question5 != null && q5path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 05', 325, 300, {align : 'left'});
          doc.image (q5path, 325, 320, {height : 200, width : 250}); 
        }

      console.log('question6path = ' + q6path);
      if(typeof(question6) != 'undefined' && question6 != null && q6path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 06', 325, 540, {align : 'left'});
          doc.image (q6path, 325, 560, {height : 200, width : 250}); 
        }

      // 페이지 2
      if(typeof(question7) != 'undefined' && question7 != null && q7path != '')
      {      
        doc.addPage();
        // 제목
        // doc.fontSize(12);
        // doc.text(grade + '학년', 30, 35, {align : 'left'}); 
        var gradeText="";
        switch(grade){
          case "1" : (gradeText = "중1"); break;
          case "2" : (gradeText = "중2"); break;
          case "3" : (gradeText = "중3"); break;
          case "4" : (gradeText = "고1"); break;
          case "5" : (gradeText = "고2"); break;
          case "6" : (gradeText = "고3"); break;
        }
        doc.fontSize(12);
        doc.text(gradeText, 30, 35, {align : 'left'}); 
        doc.fontSize(30);
        doc.text(name, 80, 15, {align : 'center'});  

        // 양식
        doc.lineCap('round')
          .moveTo(25, 60)
          .lineTo(580, 60)
          .stroke()
        doc.moveDown()

        doc.lineCap('round')
          .moveTo(300, 70)
          .lineTo(300, 770)
          .stroke()
        doc.moveDown()


        console.log('question7path = ' + q7path);
        if(typeof(question7) != 'undefined' && question7 != null && q7path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 07', 25, 60, {align : 'left'});
          doc.image (q7path, 25, 80, {height : 200, width : 250}); 
        }
        
          console.log('question8path = ' + q8path);
        if(typeof(question8) != 'undefined' && question8 != null && q8path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 08', 25, 300, {align : 'left'});
          doc.image (q8path, 25, 320, {height : 200, width : 250}); 
        }
        
        console.log('question9path = ' + q9path);
        if(typeof(question9) != 'undefined' && question9 != null && q9path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 09', 25, 540, {align : 'left'});
          doc.image (q9path, 25, 560, {height : 200, width : 250}); 
        }

        console.log('question10path = ' + q10path);
        if(typeof(question10) != 'undefined' && question10 != null && q10path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 10', 325, 60, {align : 'left'});
          doc.image (q10path, 325, 80, {height : 200, width : 250}); 
        }
          
        console.log('question11path = ' + q11path);
        if(typeof(question11) != 'undefined' && question11 != null && q11path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 11', 325, 300, {align : 'left'});
          doc.image (q11path, 325, 320, {height : 200, width : 250}); 
        }
          
        console.log('question12path = ' + q12path);  
        if(typeof(question12) != 'undefined' && question12 != null && q12path != '')
        {
          doc.fontSize(10);
          doc.text('문제 12', 325, 540, {align : 'left'}); 
          doc.image (q12path, 325, 560, {height : 200, width : 250}); 
        }
        console.log("페이지2 통과!!");
      }
      
      // 페이지 3
      if(typeof(question13) != 'undefined' && question13 != null && q13path != '')
      {
        doc.addPage();
        // 제목
        // doc.fontSize(12);
        // doc.text(grade + '학년', 30, 35, {align : 'left'});
        var gradeText="";
        switch(grade){
          case "1" : (gradeText = "중1"); break;
          case "2" : (gradeText = "중2"); break;
          case "3" : (gradeText = "중3"); break;
          case "4" : (gradeText = "고1"); break;
          case "5" : (gradeText = "고2"); break;
          case "6" : (gradeText = "고3"); break;
        }
        doc.fontSize(12);
        doc.text(gradeText, 30, 35, {align : 'left'}); 
        doc.fontSize(30);
        doc.text(name, 80, 15, {align : 'center'}); 

        // 양식
        doc.lineCap('round')
          .moveTo(25, 60)
          .lineTo(580, 60)
          .stroke()
        doc.moveDown()

        doc.lineCap('round')
          .moveTo(300, 70)
          .lineTo(300, 770)
          .stroke()
        doc.moveDown()

        console.log('question13path = ' + q13path);
        if(typeof(question13) != 'undefined' && question13 != null && q13path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 13', 25, 60, {align : 'left'});
            doc.image (q13path, 25, 80, {height : 200, width : 250}); 
          }
        
        console.log('question14path = ' + q14path);
        if(typeof(question14) != 'undefined' && question14 != null && q14path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 14', 25, 300, {align : 'left'});
            doc.image (q14path, 25, 320, {height : 200, width : 250}); 
          }
        
        console.log('question15path = ' + q15path);
        if(typeof(question15) != 'undefined' && question15 != null && q15path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 15', 25, 540, {align : 'left'});
            doc.image (q15path, 25, 560, {height : 200, width : 250}); 
          }

        console.log('question16path = ' + q16path);
        if(typeof(question16) != 'undefined' && question16 != null && q16path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 16', 325, 60, {align : 'left'});
            doc.image (q16path, 325, 80, {height : 200, width : 250}); 
          }

        console.log('question17path = ' + q17path);
        if(typeof(question17) != 'undefined' && question17 != null && q17path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 17', 325, 300, {align : 'left'});
            doc.image (q17path, 325, 320, {height : 200, width : 250}); 
          }

        console.log('question18path = ' + q18path);
        if(typeof(question18) != 'undefined' && question18 != null && q18path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 18', 325, 540, {align : 'left'});
            doc.image (q18path, 325, 560, {height : 200, width : 250}); 
          }
          console.log("페이지3 통과!!")
      }
      
      // 페이지 4
      if(typeof(question19) != 'undefined' && question19 != null && q19path != '')
      {
        doc.addPage();
        // 제목
        // doc.fontSize(12);
        // doc.text(grade + '학년', 30, 35, {align : 'left'});
        var gradeText="";
        switch(grade){
          case "1" : (gradeText = "중1"); break;
          case "2" : (gradeText = "중2"); break;
          case "3" : (gradeText = "중3"); break;
          case "4" : (gradeText = "고1"); break;
          case "5" : (gradeText = "고2"); break;
          case "6" : (gradeText = "고3"); break;
        }
        doc.fontSize(12);
        doc.text(gradeText, 30, 35, {align : 'left'}); 
        doc.fontSize(30);
        doc.text(name, 80, 15, {align : 'center'}); 

        // 양식
        doc.lineCap('round')
          .moveTo(25, 60)
          .lineTo(580, 60)
          .stroke()
        doc.moveDown()

        doc.lineCap('round')
          .moveTo(300, 70)
          .lineTo(300, 770)
          .stroke()
        doc.moveDown()

        console.log('question19path = ' + q19path);
        if(typeof(question19) != 'undefined' && question19 != null && q19path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 19', 25, 60, {align : 'left'});
            doc.image (q19path, 25, 80, {height : 200, width : 250}); 
          }

        console.log('question20path = ' + q20path);
        if(typeof(question20) != 'undefined' && question20 != null && q20path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 20', 25, 300, {align : 'left'});
            doc.image (q20path, 25, 320, {height : 200, width : 250}); 
          }
        
        console.log('question21path = ' + q21path);
        if(typeof(question21) != 'undefined' && question21 != null && q21path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 21', 25, 540, {align : 'left'});
            doc.image (q21path, 25, 560, {height : 200, width : 250}); 
          }

        console.log('question22path = ' + q22path);
        if(typeof(question22) != 'undefined' && question22 != null && q22path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 22', 325, 60, {align : 'left'});
            doc.image (q22path, 325, 80, {height : 200, width : 250}); 
          }

        console.log('question23path = ' + q23path);
        if(typeof(question23) != 'undefined' && question23 != null && q23path != '')
          { 
            doc.fontSize(10);
            doc.text('문제 23', 325, 300, {align : 'left'});
            doc.image (q23path, 325, 320, {height : 200, width : 250}); 
          }

        console.log('question24path = ' + q24path);
        if(typeof(question24) != 'undefined' && question24 != null && q24path != '')
          {
            doc.fontSize(10);
            doc.text('문제 24', 325, 540, {align : 'left'}); 
            doc.image (q24path, 325, 560, {height : 200, width : 250}); 
          }
          console.log("페이지4 통과!!")
      }
      
      // 페이지 5
      if(typeof(question25) != 'undefined' && question25 != null && q25path != '')
      {
        doc.addPage();
        // 제목
        // doc.fontSize(12);
        // doc.text(grade + '학년', 30, 35, {align : 'left'});
        var gradeText="";
        switch(grade){
          case "1" : (gradeText = "중1"); break;
          case "2" : (gradeText = "중2"); break;
          case "3" : (gradeText = "중3"); break;
          case "4" : (gradeText = "고1"); break;
          case "5" : (gradeText = "고2"); break;
          case "6" : (gradeText = "고3"); break;
        }
        doc.fontSize(12);
        doc.text(gradeText, 30, 35, {align : 'left'}); 
        doc.fontSize(30);
        doc.text(name, 80, 15, {align : 'center'});  

        // 양식
        doc.lineCap('round')
          .moveTo(25, 60)
          .lineTo(580, 60)
          .stroke()
        doc.moveDown()

        doc.lineCap('round')
          .moveTo(300, 70)
          .lineTo(300, 770)
          .stroke()
        doc.moveDown()

        console.log('question25path = ' + q25path);
        if(typeof(question25) != 'undefined' && question25 != null && q25path != '')
        {
          doc.fontSize(10);
          doc.text('문제 25', 25, 60, {align : 'left'}); 
          doc.image (q25path, 25, 80, {height : 200, width : 250}); 
        }

        console.log('question26path = ' + q26path);
        if(typeof(question26) != 'undefined' && question26 != null && q26path != '')
        { 
          doc.fontSize(10);
          doc.text('문제 26', 25, 300, {align : 'left'});
          doc.image (q26path, 25, 320, {height : 200, width : 250}); 
        }

        console.log('question27path = ' + q27path);
        if(typeof(question27) != 'undefined' && question27 != null && q27path != '')  
        { 
          doc.fontSize(10);
          doc.text('문제 27', 25, 540, {align : 'left'});
          doc.image (q27path, 25, 560, {height : 200, width : 250}); 
        }

        console.log('question28path = ' + q28path);
        if(typeof(question28) != 'undefined' && question28 != null && q28path != '')
        {
          doc.fontSize(10);
          doc.text('문제 28', 325, 60, {align : 'left'}); 
          doc.image (q28path, 325, 80, {height : 200, width : 250}); 
        }

        console.log('question29path = ' + q29path);
        if(typeof(question29) != 'undefined' && question29 != null && q29path != '')
        {
          doc.fontSize(10);
          doc.text('문제 29', 325, 300, {align : 'left'}); 
          doc.image (q29path, 325, 320, {height : 200, width : 250}); 
        }

        console.log('question30path = ' + q30path);
        if(typeof(question30) != 'undefined' && question30 != null && q30path != '')
        {
          doc.fontSize(10);
          doc.text('문제 30', 325, 540, {align : 'left'}); 
          doc.image (q30path, 325, 560, {height : 200, width : 250}); 
        }
        console.log("페이지5 통과!!")
      }
      try
      {
        console.log('path = ' + fs.existsSync(path.join(__dirname, '..', "public", "uploads", grade, 'pdf')));
      }
      catch(e)
      {
        console.error(e);
      }
            
      // console.log('doc.path =' + doc.path);
      doc.end();
    })
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  