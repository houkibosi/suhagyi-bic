const models = require('../models/models');
const moment = require('moment');

/* 태이블 정보
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    writer: Sequelize.INTEGER
*/

// 전체 찾기
exports.index = (req, res) => {
  const nGrade = parseInt(req.params.nGrade, 10);
  const nClass = parseInt(req.params.nClass, 10);
//   models.consulting.findAll({
//     where: {
//       grade: nGrade,
//       class: nClass
//     },
//     order: [['createdAt', 'DESC']]
//   },
// )

var strQuery = '';
strQuery += 'select cons.*, consre.content as replycontent, consre.writer as replywriter  ';
strQuery += 'from consultings cons ';
strQuery += 'left outer join users on cons.writer = users.userid ';
strQuery += 'left outer join consultingreplies consre on cons.id = consre.boardid ';
strQuery += 'where users.grade = '+nGrade+' and users.classname = '+nClass+' ';

models.sequelize.query(strQuery, { type: models.sequelize.QueryTypes.SELECT})
.then(data => res.status(200).json(
    {
      'data' : data
    }
  ));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.consulting.findOne({
      where: {
        id: id
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No consulting'});
      }
  
      return res.status(200).json(
      {
        'data' : data
      });
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.consulting.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const title = req.body.title || '';
    const content = req.body.content || '';
    const writer = req.body.writer || '';

    models.consulting.create({
      title: title,
      content: content,
      writer: writer
    }).then((data) => res.status(200).json(data))
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  