const models = require('../models/models');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    Studentsid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.question.findAll({
      where: {
        isteacher: 1
      }
    })
        .then(data => res.json(
          {
            'data' : data
          })
        );
};
  
// 클레스 기준 찾기
exports.show = (req, res) => {
  const questionId = req.params.questionId || '';
    if (!questionId) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.question.findAll({
      where: {
        id: questionId
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Teacher'});
      }
  
      return res.json(
        {
          'data' : data
        });
    });
};

// 삭제  
exports.destroy = (req, res) => {
  const questionId = req.body.questionId || '';
  const content = '';

  models.question.update(
  {
    commentary: content
  }, 
  {
    where: 
    {
      id:questionId
    }
  }).then((Students) => res.status(201).json(Students))
};
  
// 생성
exports.create = (req, res) => {
  const questionId = req.body.questionId || '';
  const content = req.body.content || '';

  models.question.update(
  {
    commentary: content
  }, 
  {
    where: 
    {
      id:questionId
    }
  }).then((Students) => res.status(201).json(Students))
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  