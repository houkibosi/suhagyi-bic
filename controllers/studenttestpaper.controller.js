const models = require('../models/models');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const PDF = require('pdfkit');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    Studentsid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
  let strQuery = '';
  strQuery += "select tp.id, tp.name, tp.startdate, tp.enddate, ";
  strQuery += "ifnull(sp.createdAt, \"미응시\") as status, tp.path ";
  strQuery += "from math.testpapers as tp left outer join math.studenttestpapers as sp ";
  strQuery += "on tp.id = sp.testpaperid";

  models.sequelize.query(strQuery, { type: models.sequelize.QueryTypes.SELECT})
  .then(data => {
    if (!data) {
      return res.status(404).json({error: 'No Students'});
    }

    return res.json(
      {
        'data' : data
      }
    );;
  });
};
  
// 클레스 기준 찾기
exports.show = (req, res) => {
    // const testpaperid = parseInt(req.params.testpaperid, 10);
    const userid = req.params.userid;
    const grade = req.params.grade;
    const classIndex = req.params.classIndex;
    if (!userid) {
      return res.status(400).json({error: 'Incorrect testid'});
    }
  
    // models.studentTestpaper.findAll({
    //   where: {
    //     questionid: testpaperid,
    //     studentid: userid
    //   }

    // let strQuery = '';
    // strQuery += "select tp.id, tp.name, tp.startdate, tp.enddate, ";
    // strQuery += "ifnull(sp.createdAt, \"미응시\") as status, tp.path ";
    // strQuery += "from math.testpapers as tp left outer join math.studenttestpapers as sp ";
    // strQuery += "on tp.id = sp.testpaperid where tp.tester = \""+userid+"\";";

    let strQuery = '';
    // strQuery += 'SELECT tp.id, tp.name, (select name from users where userid = tp.writer limit 1) as writername, tp.startdate, tp.enddate, tp.path, ifnull(sp.dodate, "미응시") as status ';
    // strQuery += 'SELECT tp.id, tp.name, (select name from users where userid = tp.writer limit 1) as writername, tp.startdate, tp.enddate, tp.path ';
    // strQuery += 'FROM testpapers tp left outer join studenttestpapers sp ';
    // strQuery += 'on tp.id = sp.testpaperid ';
    // strQuery += 'where tp.writer = "admin" or ';
    // strQuery += 'tp.writer = ( ';
    // strQuery += '	select userid from users ';
    // strQuery += '	where isteacher=1 and ';
    // strQuery += '	grade=(select grade from users where userid = "'+userid+'" limit 1) and ';
    // strQuery += '	classname=(select classname from users where userid = "'+userid+'" limit 1) ';
    // strQuery += ' limit 1 ';
    // strQuery += ') and ';
    // strQuery += 'tp.grade = ( ';
    // strQuery += 'select grade from users where userid = "'+userid+'" limit 1 ';
    // // strQuery += ') and !sp.dodate ';
    // strQuery += ') ';
    // strQuery += 'order by enddate desc ';
    strQuery += 'select tp.id, users.name as username, tp.name, tp.startdate, tp.enddate, tp.path, ifnull(sp.dodate, "미응시") as status ';
    strQuery += 'from testpapers tp left outer join studenttestpapers sp on sp.testpaperid = tp.id ';
    strQuery += 'left outer join users on tp.writer = users.userid ';
    strQuery += 'where tp.grade = '+grade+' and tp.class = '+classIndex+'  and sp.dodate is NULL ';


    models.sequelize.query(strQuery, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Students'});
      }
  
      return res.json(
        {
          'data' : data
        }
      );;
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.body.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.studentTestpaper.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
      const studentid = req.body.studentid || '';
      const testpaperid = req.body.testpaperid || '';
      const name = req.body.name || '';
      const startdate = req.body.startdate || '0000-00-00';
      const total = parseInt(req.body.total, 10) || null;
      let okanser = 0;
      const grade = req.body.grade || '';
      const writer = req.body.writer || '';
      let question1anser = req.body.question1anser|| null;
      let question2anser = req.body.question2anser|| null;
      let question3anser = req.body.question3anser|| null;
      let question4anser = req.body.question4anser|| null;
      let question5anser = req.body.question5anser|| null;
      let question6anser = req.body.question6anser|| null;
      let question7anser = req.body.question7anser|| null;
      let question8anser = req.body.question8anser|| null;
      let question9anser = req.body.question9anser|| null;
      let question10anser = req.body.question10anser || null;
      let question11anser = req.body.question11anser || null;
      let question12anser = req.body.question12anser || null;
      let question13anser = req.body.question13anser || null;
      let question14anser = req.body.question14anser || null;
      let question15anser = req.body.question15anser || null;
      let question16anser = req.body.question16anser || null;
      let question17anser = req.body.question17anser || null;
      let question18anser = req.body.question18anser || null;
      let question19anser = req.body.question19anser || null;
      let question20anser = req.body.question20anser || null;
      let question21anser = req.body.question21anser || null;
      let question22anser = req.body.question22anser || null;
      let question23anser = req.body.question23anser || null;
      let question24anser = req.body.question24anser || null;
      let question25anser = req.body.question25anser || null;
      let question26anser = req.body.question26anser || null;
      let question27anser = req.body.question27anser || null;
      let question28anser = req.body.question28anser || null;
      let question29anser = req.body.question29anser || null;
      let question30anser = req.body.question30anser || null;

      models.testpaper.findOne({
        where: {
          id: testpaperid
        }
      }).then(testPaper => {
        if(!testPaper)
          return '';

        // 문제 1
        models.question.findOne({
          where: {
            id: testPaper.question1
          }
        }).then(data => {
          if(!data)
            return '';

          if(question1anser)
          {
            question1anser = (data.anser === question1anser ? question1anser + "__OK" : question1anser);
            if(question1anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question1,
                wronganser: question1anser,
                questionnumber: 1,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 2
        models.question.findOne({
          where: {
            id: testPaper.question2
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question2anser)
          {
            question2anser = (data.anser === question2anser ? question2anser + "__OK" : question2anser);
            if(question2anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question2,
                wronganser: question2anser,
                questionnumber: 2,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';
        
        // 문제 3
        models.question.findOne({
          where: {
            id: testPaper.question3
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question3anser)
          {
            question3anser = (data.anser === question3anser ? question3anser + "__OK" : question3anser);
            if(question3anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question3,
                wronganser: question3anser,
                questionnumber: 3,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 4
        models.question.findOne({
          where: {
            id: testPaper.question4
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question4anser)
          {
            question4anser = (data.anser === question4anser ? question4anser + "__OK" : question4anser);
            if(question4anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question4,
                wronganser: question4anser,
                questionnumber: 4,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 5
        models.question.findOne({
          where: {
            id: testPaper.question5
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question5anser)
          {
            question5anser = (data.anser === question5anser ? question5anser + "__OK" : question5anser);
            if(question5anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question5,
                wronganser: question5anser,
                questionnumber: 5,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 6
        models.question.findOne({
          where: {
            id: testPaper.question6
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question6anser)
          {
            question6anser = (data.anser === question6anser ? question6anser + "__OK" : question6anser);
            if(question6anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question6,
                wronganser: question6anser,
                questionnumber: 6,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 7
        models.question.findOne({
          where: {
            id: testPaper.question7
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question7anser)
          {
            question7anser = (data.anser === question7anser ? question7anser + "__OK" : question7anser);
            if(question7anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question7,
                wronganser: question7anser,
                questionnumber: 7,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 8
        models.question.findOne({
          where: {
            id: testPaper.question8
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question8anser)
          {
            question8anser = (data.anser === question8anser ? question8anser + "__OK" : question8anser);
            if(question8anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question8,
                wronganser: question8anser,
                questionnumber: 8,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';
          
        // 문제 9
        models.question.findOne({
          where: {
            id: testPaper.question9
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question9anser)
          {
            question9anser = (data.anser === question9anser ? question9anser + "__OK" : question9anser);
            if(question9anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question9,
                wronganser: question9anser,
                questionnumber: 9,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';
          
        // 문제 10
        models.question.findOne({
          where: {
            id: testPaper.question10
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question10anser)
          {
            question10anser = (data.anser === question10anser ? question10anser + "__OK" : question10anser);
            if(question10anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question10,
                wronganser: question10anser,
                questionnumber: 10,
                testpaperid : testpaperid
              })
            }
          }
        }) || ''; 

        // 문제 11
        models.question.findOne({
          where: {
            id: testPaper.question11
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question10anser)
          {
            question11anser = (data.anser === question11anser ? question11anser + "__OK" : question11anser);
            if(question11anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question11,
                wronganser: question11anser,
                questionnumber: 11,
                testpaperid : testpaperid
              })
            }
          }
        }) || ''; 

        // 문제 12
        models.question.findOne({
          where: {
            id: testPaper.question12
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question12anser)
          {
            question12anser = (data.anser === question12anser ? question12anser + "__OK" : question12anser);
            if(question12anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question12,
                wronganser: question12anser,
                questionnumber: 12,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 13
        models.question.findOne({
          where: {
            id: testPaper.question13
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question13anser)
          {
            question13anser = (data.anser === question13anser ? question13anser + "__OK" : question13anser);
            if(question13anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question13,
                wronganser: question13anser,
                questionnumber: 13,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 14
        models.question.findOne({
          where: {
            id: testPaper.question14
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question14anser)
          {
            question14anser = (data.anser === question14anser ? question14anser + "__OK" : question14anser);
            if(question14anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question14,
                wronganser: question14anser,
                questionnumber: 14,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 15
        models.question.findOne({
          where: {
            id: testPaper.question15
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question15anser)
          {
            question15anser = (data.anser === question15anser ? question15anser + "__OK" : question15anser);
            if(question15anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question15,
                wronganser: question15anser,
                questionnumber: 15,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 16
        models.question.findOne({
          where: {
            id: testPaper.question16
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question16anser)
          {
            question16anser = (data.anser === question16anser ? question16anser + "__OK" : question16anser);
            if(question16anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question16,
                wronganser: question16anser,
                questionnumber: 16,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 17
        models.question.findOne({
          where: {
            id: testPaper.question17
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question17anser)
          {
            question17anser = (data.anser === question17anser ? question17anser + "__OK" : question17anser);
            if(question17anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question17,
                wronganser: question17anser,
                questionnumber: 17,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 18
        models.question.findOne({
          where: {
            id: testPaper.question18
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question18anser)
          {
            question18anser = (data.anser === question18anser ? question18anser + "__OK" : question18anser);
            if(question18anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question18,
                wronganser: question18anser,
                questionnumber: 18,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 19
        models.question.findOne({
          where: {
            id: testPaper.question19
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question19anser)
          {
            question19anser = (data.anser === question19anser ? question19anser + "__OK" : question19anser);
            if(question19anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question19,
                wronganser: question19anser,
                questionnumber: 19,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 20
        models.question.findOne({
          where: {
            id: testPaper.question20
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question20anser)
          {
            question20anser = (data.anser === question20anser ? question20anser + "__OK" : question20anser);
            if(question20anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question20,
                wronganser: question20anser,
                questionnumber: 20,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 21
        models.question.findOne({
          where: {
            id: testPaper.question21
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question21anser)
          {
            question21anser = (data.anser === question21anser ? question21anser + "__OK" : question21anser);
            if(question21anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question21,
                wronganser: question21anser,
                questionnumber: 21,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 22
        models.question.findOne({
          where: {
            id: testPaper.question22
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question22anser)
          {
            question22anser = (data.anser === question22anser ? question22anser + "__OK" : question22anser);
            if(question22anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question22,
                wronganser: question22anser,
                questionnumber: 22,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 23
        models.question.findOne({
          where: {
            id: testPaper.question23
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question23anser)
          {
            question23anser = (data.anser === question23anser ? question23anser + "__OK" : question23anser);
            if(question23anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question23,
                wronganser: question23anser,
                questionnumber: 23,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 24
        models.question.findOne({
          where: {
            id: testPaper.question24
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question24anser)
          {
            question24anser = (data.anser === question24anser ? question24anser + "__OK" : question24anser);
            if(question24anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question24,
                wronganser: question24anser,
                questionnumber: 24,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 25
        models.question.findOne({
          where: {
            id: testPaper.question25
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question25anser)
          {
            question25anser = (data.anser === question25anser ? question25anser + "__OK" : question25anser);
            if(question25anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question25,
                wronganser: question25anser,
                questionnumber: 25,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 26
        models.question.findOne({
          where: {
            id: testPaper.question26
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question26anser)
          {
            question26anser = (data.anser === question26anser ? question26anser + "__OK" : question26anser);
            if(question26anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question26,
                wronganser: question26anser,
                questionnumber: 26,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 27
        models.question.findOne({
          where: {
            id: testPaper.question27
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question27anser)
          {
            question27anser = (data.anser === question27anser ? question27anser + "__OK" : question27anser);
            if(question27anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question27,
                wronganser: question27anser,
                questionnumber: 27,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';
          
        // 문제 28
        models.question.findOne({
          where: {
            id: testPaper.question28
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question28anser)
          {
            question28anser = (data.anser === question28anser ? question28anser + "__OK" : question28anser);
            if(question28anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question28,
                wronganser: question28anser,
                questionnumber: 28,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 29
        models.question.findOne({
          where: {
            id: testPaper.question29
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question29anser)
          {
            question29anser = (data.anser === question29anser ? question29anser + "__OK" : question29anser);
            if(question29anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question29,
                wronganser: question29anser,
                questionnumber: 29,
                testpaperid : testpaperid
              })
            }
          }
        }) || '';

        // 문제 30
        models.question.findOne({
          where: {
            id: testPaper.question30
          }
        }).then(data => {
          if(!data)
            return '';
                    
          if(question30anser)
          {
            question30anser = (data.anser === question30anser ? question30anser + "__OK" : question30anser);
            if(question30anser.indexOf("__OK") != -1)
              okanser++;
            else
            {
              models.wronganser.create({
                userid: studentid,
                questionid: testPaper.question30,
                wronganser: question30anser,
                questionnumber: 30,
                testpaperid : testpaperid
              })
            }
          }
        }).then((data) => {
          console.log("student new!!");
          models.studentTestpaper.create({
            studentid : studentid,
            testpaperid : testpaperid, 
            name : name,
            grade : grade,
            startdate: startdate,
            total: total,
            okanser: okanser,
            grade: grade,
            writer: writer,
            dodate: new Date(),
            question1anser : question1anser,
            question2anser : question2anser,
            question3anser : question3anser,
            question4anser : question4anser,
            question5anser : question5anser,
            question6anser : question6anser,
            question7anser : question7anser,
            question8anser : question8anser,
            question9anser : question9anser,
            question10anser : question10anser,
            question11anser : question11anser,
            question12anser : question12anser,
            question13anser : question13anser,
            question14anser : question14anser,
            question15anser : question15anser,
            question16anser : question16anser,
            question17anser : question17anser,
            question18anser : question18anser,
            question19anser : question19anser,
            question20anser : question20anser,
            question21anser : question21anser,
            question22anser : question22anser,
            question23anser : question23anser,
            question24anser : question24anser,
            question25anser : question25anser,
            question26anser : question26anser,
            question27anser : question27anser,
            question28anser : question28anser,
            question29anser : question29anser,
            question30anser : question30anser
          })
          .then((data) => res.status(200).json(data))
        });
      })

    // models.studentTestpaper.create({
    //   studentid : studentid,
    //   testpaperid : testpaperid, 
    //   name : name,
    //   grade : grade,
    //   startdate: startdate,
    //   total: total,
    //   okanser: okanser,
    //   grade: grade,
    //   writer: writer,
    //   question1anser : question1anser,
    //   question2anser : question2anser,
    //   question3anser : question3anser,
    //   question4anser : question4anser,
    //   question5anser : question5anser,
    //   question6anser : question6anser,
    //   question7anser : question7anser,
    //   question8anser : question8anser,
    //   question9anser : question9anser,
    //   question10anser : question10anser,
    //   question11anser : question11anser,
    //   question12anser : question12anser,
    //   question13anser : question13anser,
    //   question14anser : question14anser,
    //   question15anser : question15anser,
    //   question16anser : question16anser,
    //   question17anser : question17anser,
    //   question18anser : question18anser,
    //   question19anser : question19anser,
    //   question20anser : question20anser,
    //   question21anser : question21anser,
    //   question22anser : question22anser,
    //   question23anser : question23anser,
    //   question24anser : question24anser,
    //   question25anser : question25anser,
    //   question26anser : question26anser,
    //   question27anser : question27anser,
    //   question28anser : question28anser,
    //   question29anser : question29anser,
    //   question30anser : question30anser
    // })
    // .then((data) => res.status(200).json(data))
    // .then((data) => {
      
    // })
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  