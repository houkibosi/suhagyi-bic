const models = require('../models/models');

/* 태이블 정보
    boardid: Sequelize.INTEGER,
    content: Sequelize.STRING,
    writer: Sequelize.INTEGER,
    order: sequelize.INTEGER
*/

// 전체 찾기
exports.index = (req, res) => {
    models.consultingReply.findAll()
        .then(data => res.json(data));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const boardid = parseInt(req.params.boardid, 10);
    if (!boardid) {
      return res.status(400).json({error: 'Incorrect boardid'});
    }
  
    models.consultingReply.findAll({
      where: {
        boardid: boardid
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No notireply'});
      }
  
      return res.json(data);
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const boardid = parseInt(req.params.boardid, 10);
    if (!boardid) {
      return res.status(400).json({error: 'Incorrect boardid'});
    }
  
    models.consultingReply.destroy({
      where: {
        boardid: boardid
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const boardid = req.body.boardid || '';
    const content = req.body.content || '';
    const writer = req.body.writer || '';
    const order = req.body.order || '';

    if (!boardid.length) {
      return res.status(400).json({error: 'Incorrenct boardid'});
    }
  
    models.consultingReply.create({
      boardid: boardid,
      content: content,
      writer: writer,
      order: order
    }).then((data) => res.status(201).json(data))
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  