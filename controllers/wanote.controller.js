const models = require('../models/models');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    Studentsid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.question.findAll({
    }).then(data => res.json(
      {
        'data' : data
      }
    ));
};
  
// 클레스 기준 찾기
exports.show = (req, res) => {
    const userid = req.params.userid;
    console.log(userid);
    if (!userid) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    // models.wronganser.findAll({
    //   where: {
    //     userid: userid
    //   }
    // })
    var queryStr = '';
    // queryStr += 'SELECT qs.*, tb.sValue textbookname, uc.sValue unitname, tc.sValue typename, wa.wronganser  ';
    // queryStr += 'FROM questions qs, textbookcodes tb, unitcodes uc, typeCodes tc, wrongansers wa ';
    // queryStr += 'where qs.textbook = tb.id and qs.unitCode = uc.id and qs.typeCode = tc.id and wa.questionid = qs.id and wa.userid = "'+userid+'";';

    queryStr += 'SELECT wp.name, wp.createdAt, wp.createdAt FROM math.wrongpapers wp;';
    

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => {
      console.log(data);
      if (!data) {
        return res.status(404).json({error: 'No Question'});
      }

      
  
      return res.json(
        {
          'data' : data
        }
      );
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.body.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }

    models.question.findOne({
      where: {
        id: id
      }
    }).then((data) => {
      // console.log(path.join(__dirname, data.image, data.imagename));
      console.log("__dirname = " + __dirname);
      console.log("data.image = " + data.image);
      console.log("data.image = " + data.imagename);

      try{
        fs.unlinkSync(path.join(__dirname, data.image, data.imagename))
      }
      catch(e) {
        console.log(e);
      }

      models.question.destroy({
        where: {
          id: id
        }
      })
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const findword = 'C:\\fakepath\\';
    const textbook = req.body.textbook || '';
    const multi = req.body.multi || '';
    const grade = req.body.grade || '';
    const level = req.body.level || '';
    const number = req.body.number || '';
    // const image = __dirname + "/../public/uploads/" + grade + '/' + textbook;
    const image = path.join("..", "public", "uploads", grade, textbook);
    // const imagename = path.basename(req.body.imagename);
    const imagename = req.body.imagename.replace(findword, '');
    const anser = req.body.anser || '';
    const video = req.body.video || '';
    const name = req.body.name || '';
    const writer = req.body.name || '';

    
    console.log('imagename = ' + imagename);
    //if(imagename.indexOf(findword) != -1)
    //{
      // imagename.replace(findword, '');      
    //}
    // path.basename(imagename

    // let arrayImageName = imagename.split("/");
    // imagename = arrayImageName[arrayImageName.length-1];

    console.log('imagename = ' + imagename);
  
    models.question.create({
      textbook: textbook,
      multi: multi,
      grade: grade,
      level: level,
      number: number,
      image: image,
      imagename: imagename,
      anser: anser,
      video: video,
      name: name,
      writer: writer
    }).then((data) => res.status(200).json(data))
};

// 업데이트
exports.update = (req, res) => {
  const textbook = req.body.textbook;
  const grade = req.body.grade;
  const level = req.body.level;
  const multi = req.body.multi;
  const number = req.body.number;
  const anser = req.body.anser;
  const video = req.body.video;
  const name = req.body.name;
  const writer = req.body.writer;
  const id = req.body.id;

  console.log("update id = " + id + ', name = ' + name);

  models.question.update(
    {
      textbook: textbook,
      grade: grade,
      level: level,
      multi: multi,
      number: number,
      anser: anser,
      video: video,
      name: name,
      writer: writer
    }, 
    {
      where: 
      {
        id:id
      }
    })
    .then((data) => res.status(200).json(data))
  }

  