const express = require('express');
const expressSession =  require('express-session');
const router = express.Router();
const auth = require('../auth/auth');
const moment = require('moment');
const fs = require('fs');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const mime = require('mime-types');
const path = require('path');

const iconvLite = require('iconv-lite');

const multer = require('multer');
const _storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/public/uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})
const upload = multer({storage: _storage});

const models = require('../models/models');

const formidable = require('formidable');

// =========== 페이지 라우팅 =========== 
// main Pages
router.get('/', (req, res) => {
    res.render('index.ejs', {title : 'Express'});
});
router.get('/login', (req, res) => {
    res.render('login.ejs', {title : 'Express'});
});
router.get('/index', isLoggedin, (req, res) => {
    if(req.user.isadmin === true || req.user.isteacher === true)
    {
        if(req.user.password === 'password')
        {
            res.render('admin-userinfo.ejs', {
                title : 'Express',
                user: req.user
            });
        }
        else
        {
            res.render('admin-index.ejs', {
                title : 'Express',
                user: req.user
            });
        }
    }
    else
    {
        if(req.user.password === 'password')
        {
            res.render('userinfo.ejs', {
                title : 'Express',
                user: req.user
            });
        }
        else
        {
            res.render('dashboard.ejs', {
                title : 'Express',
                user: req.user
            });
        }
    }
});

// 공지사항
router.get('/notice', isLoggedin, (req, res) => {
    res.render('notice.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 공지사항 상세보기
router.get('/notice-detail', isLoggedin, (req, res) => {
    res.render('notice-detail.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 학습관리
router.get('/studystatus', isLoggedin, (req, res) => {
    res.render('studystatus.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 사용자 정보
router.get('/userinfo', isLoggedin, (req, res) => {
    res.render('userinfo.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 오답노트
router.get('/wanote', isLoggedin, (req, res) => {
    res.render('wanote.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 오답문제
router.get('/wronganser', isLoggedin, (req, res) => {
    res.render('wronganser.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 리포트
router.get('/report', isLoggedin, (req, res) => {
    res.render('report.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 상담 게시판
router.get('/consulting', isLoggedin, (req, res) => {
    res.render('consulting.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 상담 게시판 작성
router.get('/consulting-new', isLoggedin, (req, res) => {
    res.render('consulting-new.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 상담 게시판 상세보기
router.get('/consulting-detail', isLoggedin, (req, res) => {
    res.render('consulting-detail.ejs', {
        title : 'Express',
        user: req.user
    });
});



// 관리자 Dashboard
router.get('/admin-index', isLoggedin, (req, res) => {
    res.render('admin-index.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 공지사항
router.get('/admin-notice', isLoggedin, (req, res) => {
    res.render('admin-notice.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/admin-notice-new', isLoggedin, (req, res) => {
    res.render('admin-notice-new.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/admin-notice-detail', isLoggedin, (req, res) => {
    res.render('admin-notice-detail.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 학부모 상담
router.get('/admin-consulting', isLoggedin, (req, res) => {
    res.render('admin-consulting.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/admin-consulting-new', isLoggedin, (req, res) => {
    res.render('admin-consulting-new.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/admin-consulting-detail', isLoggedin, (req, res) => {
    res.render('admin-consulting-detail.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 학생 관리
router.get('/admin-student', isLoggedin, (req, res) => {
    res.render('admin-student.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 학습지 관리
router.get('/admin-textbook', isLoggedin, (req, res) => {
    res.render('admin-textbook.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 문제소스 관리
router.get('/admin-question', isLoggedin, (req, res) => {
    res.render('admin-question.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 반 관리
router.get('/admin-class', isLoggedin, (req, res) => {
    res.render('admin-class.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 선생 관리
router.get('/admin-teacher', isLoggedin, (req, res) => {
    res.render('admin-teacher.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 시험지 관리
router.get('/admin-testpaper', isLoggedin, (req, res) => {
    res.render('admin-testpaper.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 관리자 코드 관리
router.get('/admin-codemanager', isLoggedin, (req, res) => {
    res.render('admin-codemanager.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 선생님 오답지 관리
router.get('/admin-wanote', isLoggedin, (req, res) => {
    res.render('admin-wanote.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 선생님 오답 관리
router.get('/admin-wronganser', isLoggedin, (req, res) => {
    res.render('admin-wronganser.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 선생님 오답 관리
router.get('/admin-userinfo', isLoggedin, (req, res) => {
    res.render('admin-userinfo.ejs', {
        title : 'Express',
        user: req.user
    });
});







// =========== API 라우팅 =========== 

// Noti API
const notice = require('./notice.controller');
router.get('/notices', notice.index);
router.get('/notices/:id', notice.show);
router.delete('/notices/:id', notice.destroy);
router.post('/notices', notice.create);
router.put('/notices/:id', notice.update);

// Noti Reply API
const notireply = require('./noticereply.controller');
router.get('/notireplys', notireply.index);
router.get('/notireplys/:boardid', notireply.show);
router.delete('/notireplys/:boardid', notireply.destroy);
router.post('/notireplys', notireply.create);
router.put('/notireplys/:boardid', notireply.update);

// Consulting API
const consulting = require('./consulting.controller');
router.get('/consultings', consulting.index);
router.get('/consultings/:userid', consulting.show);
router.delete('/consultings/:id', consulting.destroy);
router.post('/consultings', consulting.create);
router.put('/consultings/:id', consulting.update);

// Consulting2 API
const consulting2 = require('./consulting2.controller');
router.get('/consultings2/:nGrade/:nClass', consulting2.index);
router.get('/consultings2/:id', consulting2.show);
router.delete('/consultings2/:id', consulting2.destroy);
router.post('/consulting2s', consulting2.create);
router.put('/consultings2/:id', consulting2.update);

// Consulting Reply API
const consultingreply = require('./consultingreply.controller');
router.get('/consultingreplys', consultingreply.index);
router.get('/consultingreplys/:boardid', consultingreply.show);
router.delete('/consultingreplys/:boardid', consultingreply.destroy);
router.post('/consultingreplys', consultingreply.create);
router.put('/consultingreplys/:boardid', consultingreply.update);

// StudentClass API
const StudentClass = require('./StudentClass.controller');
router.get('/studentclass', StudentClass.index);
router.get('/studentclass/:grade', StudentClass.show);
router.delete('/studentclass', StudentClass.destroy);
router.post('/studentclass', StudentClass.create);
router.put('/studentclass', StudentClass.update);

// User API
const user = require('./user.controller');
router.get('/users', user.index);
router.get('/users/:grade/:classname', user.show);
router.delete('/users/:boardid', user.destroy);
router.post('/users', user.create);
router.put('/users', user.update);

// User2 API
const user2 = require('./user2.controller');
router.get('/users2', user2.index);
router.get('/users2/:id', user2.show);
router.delete('/users2/:boardid', user2.destroy);
router.post('/users2', user2.create);
router.put('/users2', user2.update);

// Student API
const student = require('./student.controller');
router.get('/students', student.index);
router.get('/students/:grade/:classname', student.show);
router.delete('/students/:boardid', student.destroy);
router.post('/students', student.create);
router.put('/students/:boardid', student.update);

// Teacher API
const teacher = require('./teacher.controller');
router.get('/teachers', teacher.index);
router.get('/teachers/:grade/:classname', teacher.show);
router.delete('/teachers/:boardid', teacher.destroy);
router.post('/teachers', teacher.create);
router.put('/teachers/:boardid', teacher.update);

// Teacher2 API
const teacher2 = require('./teacher2.controller');
router.get('/teachers2', teacher2.index);
router.get('/teachers2/:grade', teacher2.show);
router.delete('/teachers2/:boardid', teacher2.destroy);
router.post('/teachers2', teacher2.create);
router.put('/teachers2/:boardid', teacher2.update);

// Textbook API
const Textbook = require('./Textbook.controller');
router.get('/textbooks', Textbook.index);
router.get('/textbooks/:grade', Textbook.show);
router.delete('/textbooks', Textbook.destroy);
router.post('/textbooks', Textbook.create);
router.put('/textbooks', Textbook.update);

// Quest API
const question = require('./Question.controller');
router.get('/questions/:grade/:textbook/:unitCode/:typeCode/:level', question.index);
router.get('/questions/:grade/:textbook', question.show);
router.delete('/questions', question.destroy);
router.post('/questions', question.create);
router.put('/questions', question.update);

// Quest2 API
const question2 = require('./Question2.controller');
router.get('/questions2', question2.index);
router.get('/questions2/:id', question2.show);
router.delete('/questions2', question2.destroy);
router.post('/questions2', question2.create);
router.put('/questions2', question2.update);

// Quest3 API
const question3 = require('./Question2.controller');
router.get('/questions3', question3.index);
router.get('/questions3/:userid', question3.show);
router.delete('/questions3', question3.destroy);
router.post('/questions3', question3.create);
router.put('/questions3', question3.update);

// Testpaper API
const testpaper = require('./testpaper.controller');
router.get('/testpapers', testpaper.index);
router.get('/testpapers/:grade/:userid', testpaper.show);
router.delete('/testpapers', testpaper.destroy);
router.post('/testpapers', testpaper.create);
router.put('/testpapers/:boardid', testpaper.update);

// Testpaper2 API
const testpaper2 = require('./testpaper2.controller');
router.get('/testpapers2', testpaper2.index);
router.get('/testpapers2/:testpaperid', testpaper2.show);
router.delete('/testpapers2', testpaper2.destroy);
router.post('/testpapers2', testpaper2.create);
router.put('/testpapers2/:boardid', testpaper2.update);

// Student Testpaper API
const studenttestpaper = require('./studenttestpaper.controller');
router.get('/studenttestpapers', studenttestpaper.index);
// router.get('/studenttestpapers/:testpaperid/:userid', studenttestpaper.show);
router.get('/studenttestpapers/:grade/:classIndex/:userid', studenttestpaper.show);
router.delete('/studenttestpapers', studenttestpaper.destroy);
router.post('/studenttestpapers', studenttestpaper.create);
router.put('/studenttestpapers/:boardid', studenttestpaper.update);

// WrongAnser API
const wronganser = require('./wronganser.controller');
router.get('/wrongansers', wronganser.index);
router.get('/wrongansers/:userid', wronganser.show);
router.delete('/wrongansers', wronganser.destroy);
router.post('/wrongansers', wronganser.create);
router.put('/wrongansers/:id', wronganser.update);

// WrongAnser2 API
const wronganser2 = require('./wronganser2.controller');
router.get('/wrongansers2', wronganser2.index);
router.get('/wrongansers2/:userid', wronganser2.show);
router.delete('/wrongansers2', wronganser2.destroy);
router.post('/wrongansers2', wronganser2.create);
router.put('/wrongansers2/:id', wronganser2.update);


// Wrongpaper API
const wrongpaper = require('./wrongpaper.controller');
router.get('/wrongpapers', wrongpaper.index);
router.get('/wrongpapers/:grade/:userid', wrongpaper.show);
router.delete('/wrongpapers', wrongpaper.destroy);
router.post('/wrongpapers', wrongpaper.create);
router.put('/wrongpapers/:boardid', wrongpaper.update);

// Wrongpaper2 API
const wrongpaper2 = require('./wrongpaper2.controller');
router.get('/wrongpapers2', wrongpaper2.index);
router.get('/wrongpapers2/:userid', wrongpaper2.show);
router.delete('/wrongpapers2', wrongpaper2.destroy);
router.post('/wrongpapers2', wrongpaper2.create);
router.put('/wrongpapers2/:boardid', wrongpaper2.update);

// Report API
const report = require('./report.controller');
router.get('/reports', report.index);
router.get('/reports/:userid', report.show);
router.delete('/reports', report.destroy);
router.post('/reports', report.create);
router.put('/reports/:id', report.update);

// Code Textbook API
const codetextbook = require('./code-textbook.controller');
router.get('/codetextbooks/:grade', codetextbook.index);
router.get('/codetextbooks/:id/:temp', codetextbook.show);
router.delete('/codetextbooks/:id', codetextbook.destroy);
router.post('/codetextbooks', codetextbook.create);
router.put('/codetextbooks', codetextbook.update);

// Code Unit API
const codeunit = require('./code-unit.controller');
router.get('/codeunits/:grade', codeunit.index);
router.get('/codeunits/:id/:temp', codeunit.show);
router.delete('/codeunits/:id', codeunit.destroy);
router.post('/codeunits', codeunit.create);
router.put('/codeunits', codeunit.update);

// Code Type API
const codetype = require('./code-type.controller');
router.get('/codetypes/:grade', codetype.index);
router.get('/codetypes/:id/:temp', codetype.show);
router.delete('/codetypes/:id', codetype.destroy);
router.post('/codetypes', codetype.create);
router.put('/codetypes', codetype.update);

// wanote API
const wanote = require('./wanote.controller');
router.get('/wanotes', wanote.index);
router.get('/wanotes/:userid', wanote.show);
router.delete('/wanotes', wanote.destroy);
router.post('/wanotes', wanote.create);
router.put('/wanotes', wanote.update);

// 해설 추가
const commentary = require('./commentary.controller');
router.get('/commentarys', commentary.index);
router.get('/commentarys/:questionId', commentary.show);
router.delete('/commentarys', commentary.destroy);
router.post('/commentarys', commentary.create);
router.put('/commentarys', commentary.update);

// upload
router.post('/uploads', (req, res) => {
    var form = new formidable();
    
    // form.uploadDir = __dirname + "/../public/uploads/temp";
    form.uploadDir = path.join(__dirname, "..", "public", "uploads", "temp");

    // D:\수학의빛\git\controllers/../public/uploads/temp

    // form.uploadDir = "/uploads/temp";
    // form.uploadDir = path.join("uploads", temp);
    // path.normalize(form.uploadDir);
    var oldPath = '';
    var oldPath2 = '';
    var fileName = '';
    var newfilename = "";
    var lastfile = '';
    form.keepExtensions = true;

    
    form.on('error', function(err) {
        console.log(err);
        throw err;
    })

    form.on('field', function(field, value) {
        console.log("-----------field------------");
        console.log(field);
        console.log("-----------field------------");
        console.log("-----------value------------");
        console.log(value);
        console.log("-----------value------------");
        if(field == "resultUpload")
        {
            form.uploadDir = value;
        }
        else if(field == "newFileName")
        {
            newfilename = value;
        }
    })

    form.on('fileBegin', function(name, file) {      
        // file.path = form.uploadDir + "/" + file.name;
        file.path = path.join(form.uploadDir, file.name);
        console.error("Join path ->>> ", file.path);
        // path.normalize(file.path);
        oldPath = file.path;
        oldPath2 = form.uploadDir;
        fileName = file.name;
        res.json(file.name);
    })

    form.on('file', function(field, file) {
        // console.log(file);
    })

    form.on('progress', function(bytesReceived, bytesExpected) {
        // console.log('progress');

        var percent = (bytesReceived / bytesExpected * 100) | 0;
        process.stdout.write('uploading : %' + percent + '\r');
    })

    form.on('end', function(req, res) {
        try
        { 
            console.log("try");
            if(form.uploadDir !== '')
                {
                    lastfile = path.join(__dirname, form.uploadDir);
                    fs.mkdirSync(lastfile);
                }
            else
                console.log('form.uploadDir is empty.');
        }
        catch(e)
        { 
            console.log("catch");
            if ( e.code != 'EEXIST' ) 
                // throw e; // 존재할경우 패스처리함. 
            {
                console.log(e);
            }
        }
        console.log("lastfile : ");
        console.log(lastfile);
        try
        {
            console.log("rename");
            if(newfilename)
            {
                console.log("newfilename");
                console.log(oldPath);
                console.log(path.join(lastfile, newfilename));
                fs.rename(oldPath, path.join(lastfile, newfilename), function (err) { 
                    if (err) throw err; 
                    console.log('renamed complete');
                });   
                // fs.rename(oldPath, path.join(lastfile, fileName), function (err) { 
                //     if (err) throw err; 
                //     console.log('renamed complete');
                // });   
            }
            else
            {
                console.log("fileName");
                fs.rename(oldPath, path.join(lastfile, fileName), function (err) { 
                    if (err) throw err; 
                    console.log('renamed complete');
                });
            }
        }
        catch(e)
        {
            console.log(e);
        }
        console.log('lastfile + newfilename = ' + path.join(lastfile, newfilename));
        console.log('form end: \n\n');
    });

    form.parse(req, function(err){
        console.log('form parse : \n\n');
    });
});

// 선생님 사진 upload
router.post('/teacherpicup', (req, res) => {
    var form = new formidable();
    
    // form.uploadDir = __dirname + "/../public/uploads/temp";
    form.uploadDir = path.join(__dirname, "..", "public", "teachers");

    // D:\수학의빛\git\controllers/../public/uploads/temp

    // form.uploadDir = "/uploads/temp";
    // form.uploadDir = path.join("uploads", temp);
    // path.normalize(form.uploadDir);
    var oldPath = '';
    var fileName = '';
    var lastfile = '';
    form.keepExtensions = true;

    console.log(req.file);

    
    form.on('error', function(err) {
        console.log(err);
        throw err;
    })

    form.on('field', function(field, value) {
        form.uploadDir = value;
    })

    form.on('fileBegin', function(name, file) {      
        // file.path = form.uploadDir + "/" + file.name;
        file.path = path.join(form.uploadDir, file.name);
        console.error("Join path ->>> ", file.path);
        // path.normalize(file.path);
        oldPath = file.path;
        fileName = file.name;
        res.json(file.name);        
    })

    form.on('file', function(field, file) {
        // console.log(file);
    })

    form.on('progress', function(bytesReceived, bytesExpected) {
        // console.log('progress');

        var percent = (bytesReceived / bytesExpected * 100) | 0;
        process.stdout.write('uploading : %' + percent + '\r');
    })

    form.on('end', function(req, res) {
        try
        { 
            if(form.uploadDir !== '')
                {
                    // lastfile = path.join(__dirname, form.uploadDir);
                    lastfile = form.uploadDir;
                    fs.mkdirSync(lastfile);
                }
            else
                console.log('form.uploadDir is empty.');
        }
        catch(e)
        { 
            if ( e.code != 'EEXIST' ) 
                throw e; // 존재할경우 패스처리함. 
        }
        fs.rename(oldPath, path.join(lastfile, fileName));
        console.log('lastfile + fileName = ' + path.join(lastfile, fileName));
        console.log('form end: \n\n');
    });

    form.parse(req, function(err){
        console.log('form parse : \n\n');
    });
});

//  img
router.get('/img/:id', function(req, res){
    const id = req.params.id;
    let imgpath = '';
    let imgname = '';
    let filepath = '';
    let mimetype = '';

    models.question.findOne({
        where: {
          id: id
        }
      }).then(data => {
        imgpath = data.image;
        imgname = data.imagename;
        // filepath = path + '/' + name;
        filepath = path.join(__dirname, imgpath, imgname);
        // path.normalize(filepath);
        mimetype = mime.lookup('application/octet-stream');

        // console.log('filepath = ' + filepath);

        // res.setHeader('Content-disposition', 'attachment; filename=' + name);
        // res.setHeader('Content-type', mimetype);
        // var filestream = fs.createReadStream(filepath);
        // filestream.pipe(res);

        fs.readFile(filepath, function(err, data){
            console.log(filepath);
            res.writeHead(200, {'Content-Type' : 'text/html'});
            res.end(data);
        });
      });

    // console.log('filepath = ' + filepath);

    // fs.readFile(filepath, function(err, data){
    //     res.writeHead(200, {'Content-Type' : 'text/html'});
    //     res.end(data);
    // });
});

//  img2
router.get('/img2/:id', function(req, res){
    const id = req.params.id;
    let imgpath = '';
    let imgname = '';
    let filepath = '';
    let mimetype = '';

    models.question.findOne({
        where: {
          id: id
        }
      }).then(data => {
        imgpath = data.comm;
        imgname = data.commname;
        // filepath = path + '/' + name;
        filepath = path.join(__dirname, imgpath, imgname);
        // path.normalize(filepath);
        mimetype = mime.lookup('application/octet-stream');

        // console.log('filepath = ' + filepath);

        // res.setHeader('Content-disposition', 'attachment; filename=' + name);
        // res.setHeader('Content-type', mimetype);
        // var filestream = fs.createReadStream(filepath);
        // filestream.pipe(res);

        fs.readFile(filepath, function(err, data){
            console.log(filepath);
            res.writeHead(200, {'Content-Type' : 'text/html'});
            res.end(data);
        });
      });

    // console.log('filepath = ' + filepath);

    // fs.readFile(filepath, function(err, data){
    //     res.writeHead(200, {'Content-Type' : 'text/html'});
    //     res.end(data);
    // });
});

// 오답노트 다운
router.get('/wanotedownload/:filename', function(req, res){
    const filename = req.params.filename;
    console.log(filename);
    filepath = path.join(__dirname, "..", "public", "uploads", "wanote", filename);
    // mimetype = mime.lookup('application/octet-stream');
    mimetype = mime.extension('application/octet-stream');
    res.setHeader('Content-disposition', 'attachment; filename=' + getDownloadFilename(req, filename));
    res.setHeader('Content-type', mimetype);
    var filestream = fs.createReadStream(filepath);
    filestream.pipe(res);


    // res.download(filepath);
})

// 해설 다운
router.get('/commdownload/:filename', function(req, res){
    const filename = req.params.filename;
    console.log(filename);
    filepath = path.join(__dirname, "..", "public", "uploads", "commentary", filename);
    // mimetype = mime.lookup('application/octet-stream');
    mimetype = mime.extension('application/octet-stream');
    res.setHeader('Content-disposition', 'attachment; filename=' + getDownloadFilename(req, filename));
    res.setHeader('Content-type', mimetype);
    var filestream = fs.createReadStream(filepath);
    filestream.pipe(res);


    // res.download(filepath);
})

router.get('/download/:grade/:type/:name', function(req, res){
    const filename = req.params.name;
    const grade = req.params.grade;
    const type = req.params.type;
    // let filepath = __dirname + "/../public/uploads/" + grade + '/pdf/' + filename;
    let filepath = path.join(__dirname, "..", "public", "uploads",grade, type, filename);
    console.log("filepath = " + filepath);
    // path.normalize(filepath);
    // console.log(mime);
    // mimetype = mime.lookup('application/octet-stream');
    mimetype = mime.extension('application/octet-stream');
    res.setHeader('Content-disposition', 'attachment; filename=' + getDownloadFilename(req, filename));
    res.setHeader('Content-type', mimetype);
    var filestream = fs.createReadStream(filepath);
    filestream.pipe(res);


    // res.download(filepath);
})

// 한글파일 다운 시
function getDownloadFilename(req, filename) {
    var header = req.headers['user-agent'];
 
    if (header.includes("MSIE") || header.includes("Trident")) { 
        return encodeURIComponent(filename).replace(/\\+/gi, "%20");
    } else if (header.includes("Chrome")) {
        return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), 'ISO-8859-1');
    } else if (header.includes("Opera")) {
        return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), 'ISO-8859-1');
    } else if (header.includes("Firefox")) {
        return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), 'ISO-8859-1');
    }
 
    return filename;
}

// IP리스트
router.get('/iplists/:userid', function(req, res){
    const userid = req.params.userid;
    var queryStr = "";
    queryStr += 'SELECT * FROM loginhistories ';
    queryStr += 'where userid = "'+userid+'" ';
    queryStr += 'order by createdAt desc limit 3; ';

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => {
      console.log(data);
      if (!data) {
        return res.status(404).json({error: 'No Question'});
      }

      return res.status(200).json(
        {
          'data' : data
        }
      );
    });
})

// login API
const login = require('./login.controller');
//router.post('/login', passport.authenticate('local', {successRedirect:'/index', failureRedirect: '/'}));
router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/index',
    failureRedirect: '/'
}));
router.put('/login/:id', login.update);
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

// 인증 관리
function isLoggedin (req, res, next) 
{
    //console.log("User : " + req.user);
    //console.log(req);
    console.log("\t\tisLoggedin()!!")
    console.log(req.isAuthenticated());
    console.log(req.user);
    if (req.isAuthenticated())
    {
        return next();
    } 
    else 
    {
        res.redirect('/');
    }
}

// export
module.exports = router;
