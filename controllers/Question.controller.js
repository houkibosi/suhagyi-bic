const models = require('../models/models');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    Studentsid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
  const grade = parseInt(req.params.grade, 10);

  const textbook = parseInt(req.params.textbook, 10) || 0;
  const unitCode = parseInt(req.params.unitCode, 10) || 0;
  const typeCode = parseInt(req.params.typeCode, 10) || 0;
  const level = req.params.level || '';

  var strQuery = '';
  // strQuery += 'SELECT qs.*, tb.sValue textbookname, uc.sValue unitname, tc.sValue typename ';
  // strQuery += 'FROM questions qs, textbookcodes tb, unitCodes uc, typeCodes tc ';
  // strQuery += 'where grade = '+grade+' and qs.textbook = tb.id and qs.unitCode = uc.id and qs.typeCode = tc.id;';
  // strQuery += 'where grade = '+grade+' and qs.textbook = tb.id and qs.unitCode = uc.id and qs.typeCode = tc.id;'
  strQuery += 'SELECT qs.*, tb.sValue textbookname, uc.sValue unitname, tc.sValue typename ';
  strQuery += 'FROM questions qs ';
  strQuery += 'left outer join textbookcodes tb ';
  strQuery += 'on qs.textbook = tb.id ';
  strQuery += 'left outer join unitCodes uc ';
  strQuery += 'on qs.unitCode = uc.id ';
  strQuery += 'left outer join typeCodes tc ';
  strQuery += 'on qs.typeCode = tc.id ';
  strQuery += 'where qs.grade = '+grade+' ';
  if(textbook != 0)
    {strQuery += 'and qs.textbook = '+textbook+' ';}
  if(unitCode != 0)
    {strQuery += 'and qs.unitCode = '+unitCode+' ';}
  if(typeCode != 0)
    {strQuery += 'and qs.typeCode = '+typeCode+' ';}
  if(level != '' && level != '0')
    {strQuery += 'and qs.level = '+level+' ';}
  strQuery += ';';

  models.sequelize.query(strQuery, { type: models.sequelize.QueryTypes.SELECT})    
  .then(data => res.json(
      {
        'data' : data
      }
    ));
};
  
// 클레스 기준 찾기
exports.show = (req, res) => {
    const grade = parseInt(req.params.grade, 10);
    const textbook = parseInt(req.params.textbook, 10);
    if (!grade) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    // models.question.findAll({
    //   where: {
    //     grade: grade,
    //     textbook: textbook
    //   }
    // })

    var strQuery = '';
    strQuery += 'SELECT qs.*, tb.sValue textbookname, uc.sValue unitname, tc.sValue typename ';
    strQuery += 'FROM questions qs, textbookcodes tb, unitCodes uc, typeCodes tc ';
    strQuery += 'where grade = '+grade+' and textbook = '+textbook+' and qs.textbook = tb.id and qs.unitCode = uc.id and qs.typeCode = tc.id;';

    models.sequelize.query(strQuery, { type: models.sequelize.QueryTypes.SELECT})  
    .then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Students'});
      }
  
      return res.json(
        {
          'data' : data
        }
      );
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.body.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }

    models.question.findOne({
      where: {
        id: id
      }
    }).then((data) => {
      // console.log(path.join(__dirname, data.image, data.imagename));
      console.log("__dirname = " + __dirname);
      console.log("data.image = " + data.image);
      console.log("data.image = " + data.imagename);

      try{
        fs.unlinkSync(path.join(__dirname, data.image, data.imagename))
      }
      catch(e) {
        console.log(e);
      }

      models.question.destroy({
        where: {
          id: id
        }
      })
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const nowDate = new Date().toISOString().replace(/T/, '_').replace(/\..+/, '').replace(/:/, '').replace(/:/, '');

    const findword = 'C:\\fakepath\\';
    const textbook = req.body.textbook || '';
    const grade = req.body.grade || '';
    const level = req.body.level || '';
    const number = req.body.number || '';
    // const image = __dirname + "/../public/uploads/" + grade + '/' + textbook;
    const image = path.join("..", "public", "uploads", grade, textbook);
    // const imagename = path.basename(req.body.imagename);
    // const imagename = req.body.imagename.replace(findword, '');
    const imagename = nowDate + path.extname(req.body.imagename);
    const comm = path.join("..", "public", "uploads", grade, textbook);
    // const commname = req.body.commname.replace(findword, '');
    const commname = "C_" + nowDate + path.extname(req.body.commname);
    const anser = req.body.anser || '';
    const video = req.body.video || '';
    const name = req.body.name || '';
    const writer = req.body.name || '';

    const unitCode = req.body.unitCode || '';
    const typeCode = req.body.typeCode || '';
    const commentary = '';

    const imagesize = req.body.imagesize || '';
    const commsize = req.body.commsize || '';

    
    console.log('imagename = ' + imagename);
    //if(imagename.indexOf(findword) != -1)
    //{
      // imagename.replace(findword, '');      
    //}
    // path.basename(imagename

    // let arrayImageName = imagename.split("/");
    // imagename = arrayImageName[arrayImageName.length-1];

    console.log('imagename = ' + imagename);
  
    models.question.create({
      textbook: textbook,
      grade: grade,
      level: level,
      number: number,
      image: image,
      imagename: imagename,
      comm: comm,
      commname: commname,
      video: video,
      name: name,
      writer: writer,
      unitCode: unitCode,
      typeCode: typeCode,
      anser:anser,
      commentary: commentary,
      imagesize: imagesize,
      commsize: commsize
    }).then((data) => res.status(200).json(data))
};

// 업데이트
exports.update = (req, res) => {
  const textbook = req.body.textbook;
  const grade = req.body.grade;
  const level = req.body.level;
  const multi = req.body.multi;
  const number = req.body.number;
  const anser = req.body.anser;
  const video = req.body.video;
  const name = req.body.name;
  const writer = req.body.writer;
  const id = req.body.id;

  console.log("update id = " + id + ', name = ' + name);

  models.question.update(
    {
      textbook: textbook,
      grade: grade,
      level: level,
      multi: multi,
      number: number,
      anser: anser,
      video: video,
      name: name,
      writer: writer
    }, 
    {
      where: 
      {
        id:id
      }
    })
    .then((data) => res.status(200).json(data))
  }

  