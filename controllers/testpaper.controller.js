const models = require('../models/models');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const PDF = require('pdfkit');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    Studentsid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.testpaper.findAll({})
    // models.sequelize.query("SELECT distinct a.name as name, a.startdate, a.enddate, a.writer, a.path FROM testpapers a;")
    .then(data => res.json(
      {
        'data' : data
      }
    ));
};
  
// 클레스 기준 찾기
exports.show = (req, res) => {
    const grade = parseInt(req.params.grade, 10);
    const userid = req.params.userid;
    if (!grade) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.testpaper.findAll({
      where: {
        grade: grade,
        writer: userid
      }
    })
    // models.sequelize.query('SELECT distinct a.name as name, a.startdate, a.enddate, a.writer, a.path FROM testpapers a where grade='+grade+' and writer = "'+userid+'";')
    .then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Students'});
      }
  
      return res.json(
        {
          'data' : data
        }
      );;
    });
};

// 삭제  
exports.destroy = (req, res) => {
  const id = parseInt(req.body.id, 10);
  if (!id) {
    return res.status(400).json({
      error: 'Incorrect id'
    });
  }

  models.testpaper.destroy({
    where: {
      id: id
    }
  }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const name = req.body.name || '';
  const startdate = req.body.startdate || '0000-00-00';
  const enddate = req.body.enddate || '0000-00-00';
  const grade = req.body.grade || '';
  const classIndex = parseInt(req.body.classIndex, 10) || '';
  const writer = req.body.writer || '';
  const total = req.body.total || 0;

  var questions = req.body.questions || '';
  for (var i = 0; i < questions.length; i++) 
  {
    questions[i] = parseInt(req.body.questions[i], 10) || null;
  }

  var qpath = [];
  var qsize = [];
  var questionpath = [];
  for (var i = 0; i < questions.length; i++) 
  {
    if (questions[i] !== null) 
    {
      questionpath[i] = models.question.findOne(
        {
        where: 
        {
          id: questions[i]
        }
      }).then(data => 
        {
        if (data) 
        {
          qpath[i] = path.join(__dirname, data.image, data.imagename);
          qsize[i] = data.imagesize;
        }

      }) || '';
    }
  }

  models.testpaper.create(
    {
      name: name,
      grade: grade,
      class: classIndex,
      startdate: startdate,
      enddate: enddate,
      path: path.join(__dirname, '..', 'public', 'uploads', grade, 'pdf', name + '.pdf'),
      writer: writer,

      total: total,
      question1: questions[0],
      question2: questions[1],
      question3: questions[2],
      question4: questions[3],
      question5: questions[4],
      question6: questions[5],
      question7: questions[6],
      question8: questions[7],
      question9: questions[8],
      question10: questions[9],
      question11: questions[10],
      question12: questions[11],
      question13: questions[12],
      question14: questions[13],
      question15: questions[14],
      question16: questions[15],
      question17: questions[16],
      question18: questions[17],
      question19: questions[18],
      question20: questions[19],
      question21: questions[20],
      question22: questions[21],
      question23: questions[22],
      question24: questions[23],
      question25: questions[24],
      question26: questions[25],
      question27: questions[26],
      question28: questions[27],
      question29: questions[28],
      question30: questions[29]
    })
    .then((data) => res.status(200).json(data))
    .then((data) => {
      let doc = new PDF();

      try 
      
      {
        if (path.join(__dirname, '..', "public", "uploads", grade, 'pdf') !== '') 
        {
          lastfile = path.join(__dirname, '..', "public", "uploads", grade, 'pdf');
          fs.mkdirSync(lastfile);
          console.log("Make folder = " + lastfile);
        } 
        else
        {
          console.log('Folder is empty.');
        }
      } 
      catch (e) 
      {
        if (e.code != 'EEXIST')
          throw e; // 존재할경우 패스처리함. 
      }

      console.log("Doc Pipe");
      doc.pipe(fs.createWriteStream(path.join(__dirname, '..', "public", "uploads", grade, 'pdf', name + '.pdf')));

      console.log("Doc Font");
      doc.font(path.join(__dirname, '..', 'public', 'font', 'Spoqa Han Sans Regular.ttf'));

      var spaceCount = 0;
      var imagePointX = 0;
      var imagePointY = 0;

      for (var i = 0; i < 30; i++) 
      {
        console.log("i = " + i);
        console.log("i size = " + qsize[i]);
        // spaceCount += data.

        if (spaceCount === 0) 
        {
          console.log("write line");
          // 제목
          var gradeText = "";
          switch (grade) {
            case "1":
              (gradeText = "중1");
              break;
            case "2":
              (gradeText = "중2");
              break;
            case "3":
              (gradeText = "중3");
              break;
            case "4":
              (gradeText = "고1");
              break;
            case "5":
              (gradeText = "고2");
              break;
            case "6":
              (gradeText = "고3");
              break;
          }
          doc.fontSize(12);
          doc.text(gradeText, 30, 35, {
            align: 'left'
          });
          doc.fontSize(30);
          doc.text(name, 80, 15, {
            align: 'center'
          });

          // 양식
          doc.lineCap('round')
            .moveTo(25, 60)
            .lineTo(580, 60)
            .stroke()
          doc.moveDown()

          doc.lineCap('round')
            .moveTo(300, 70)
            .lineTo(300, 770)
            .stroke()
          doc.moveDown()

          spaceCount++;
        }

        if (spaceCount === 1) 
        {
          imagePointX = 25;
          imagePointY = 80;
        }
        else if(spaceCount === 2)
        {
          imagePointX = 25;
          imagePointY = 320;
        }
        else if(spaceCount === 3)
        {
          imagePointX = 25;
          imagePointY = 540;
        }
        else if(spaceCount === 4)
        {
          imagePointX = 325;
          imagePointY = 80;
        }
        else if(spaceCount === 5)
        {
          imagePointX = 325;
          imagePointY = 320;
        }
        else if(spaceCount === 6)
        {
          imagePointX = 325;
          imagePointY = 540;
        }

        // 내용
        // if (typeof (questions[i]) != 'undefined' && questions[i] != null && qpath[i] != '') 
        // {
          
        //   doc.fontSize(10);
        //   doc.text('문제 ' + ((i + 1) / 10 > 1 ? "0" + (i + 1) : i + 1), 25, 60, {
        //     align: 'left'
        //   });
        //   doc.image(qpath[i], 25, 80, {
        //     height: 200,
        //     width: 250
        //   });
        // }
      }
      doc.end();
    })
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  