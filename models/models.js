const Sequelize = require('sequelize');
const sequelize = new Sequelize(

    // AWS
    // 'ebdb', 
    // 'klog2018', 
    // 'korea2018', 
    // {
    //     host: 'aa12cls3lojg554.c3zoboj8dpso.ap-northeast-2.rds.amazonaws.com',
    //     port: 3306,
    //     dialect: 'mysql',
    //     timezone: "+09:00",
    //     charset: 'utf8',
    //     collate: 'utf8_general_ci', 
    //     timestamps: true
    // }


    // cafe24
    'hightopmath', 
    'hightopmath', 
    'Korea@2017!', 
    {
        host: '10.0.0.1',
        port: 3306,
        dialect: 'mysql',
        operatorsAliases: false
    }


    //Local
    // 'math', 
    // 'root', 
    // 'p@ssw0rd', 
    // {
    //     host: '127.0.0.1',
    //     port: 3306,
    //     dialect: 'mysql'
    // }    
);

// 학생 정보 테이블
const User = sequelize.define('user', {
    grade: {
        type: Sequelize.INTEGER,
        field: 'grade'
    },
    classname: {
        type: Sequelize.STRING,
        field: 'classname'
    },
    classnameIndex: {
        type: Sequelize.INTEGER,
        field: 'classnameIndex'
    },
    userid: {
        type: Sequelize.STRING,
        field: 'userid'
    },
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    password: Sequelize.STRING,
    isadmin: Sequelize.BOOLEAN,
    isteacher: Sequelize.BOOLEAN,
    isstudent: Sequelize.BOOLEAN,
    isparent: Sequelize.BOOLEAN,
    image: Sequelize.STRING,
    imagename: Sequelize.STRING
});

// 반 정보 테이블
const StudentClass = sequelize.define('StudentClass', {
    grade : {
        type: Sequelize.INTEGER,
        field: 'grade'
    },
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    explanation : {
        type: Sequelize.STRING,
        field: 'explanation'
    }
});

// 공지사항
const notiBoard = sequelize.define('notiboard', {
    title: {
        type: Sequelize.STRING,
        field: 'title',
        allowNull: false
    },
    content: {
        type: Sequelize.STRING,
        field: 'content',
        allowNull: false
    },
    writer: Sequelize.STRING
})

// 공지사항 댓글
const notiReply = sequelize.define('notireply', {
    boardid: {
        type: Sequelize.INTEGER,
        field: 'boardid'
    },
    content: {
        type: Sequelize.STRING,
        field: 'content',
        allowNull: false
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer'
    }
})

// 상담 게시판
const consulting = sequelize.define('consulting', {
    title: {
        type: Sequelize.STRING,
        field: 'title',
        allowNull: false
    },
    content: {
        type: Sequelize.STRING,
        field: 'content',
        allowNull: false
    },
    writer: Sequelize.STRING,
    writername: Sequelize.STRING
})

// 상담 게시판 댓글
const consultingReply = sequelize.define('consultingreply', {
    boardid: {
        type: Sequelize.INTEGER,
        field: 'boardid'
    },
    content: {
        type: Sequelize.STRING,
        field: 'content',
        allowNull: false
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer'
    }
})

// 교재 테이블
const textbook = sequelize.define('textbook', {
    grade : {
        type: Sequelize.INTEGER,
        field: 'grade'
    },
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    code: {
        type: Sequelize.STRING,
        field: 'code'
    },
    mine: {
        type: Sequelize.STRING,
        field: 'mine'
    }
});

// 문제 테이블
const question = sequelize.define('question', {
    grade : {
        type: Sequelize.INTEGER,
        field: 'grade'
    },
    textbook : {
        type: Sequelize.INTEGER,
        field: 'textbook'
    },
    unitCode : {
        type: Sequelize.INTEGER,
        field: 'unitCode'
    },
    typeCode : {
        type: Sequelize.INTEGER,
        field: 'typeCode'
    },
    level : {
        type: Sequelize.STRING,
        field: 'level'
    },
    number : {
        type: Sequelize.STRING,
        field: 'number'
    },
    image : {
        type: Sequelize.STRING,
        field: 'image'
    },
    imagename : {
        type: Sequelize.STRING,
        field: 'imagename'
    },
    comm : {
        type: Sequelize.STRING,
        field: 'comm'
    },
    commname : {
        type: Sequelize.STRING,
        field: 'commname'
    },
    anser: {
        type: Sequelize.STRING,
        field: 'anser'
    },
    video: {
        type: Sequelize.STRING,
        field: 'video'
    },
    name: {
        type: Sequelize.STRING,
        field: 'name'
    }, 
    writer: {
        type: Sequelize.STRING,
        field: 'writer'
    },
    commentary: {
        type: Sequelize.STRING,
        field: 'commentary'
    },
    imagesize: {
        type: Sequelize.INTEGER,
        field: 'imagesize'
    },
    commsize: {
        type: Sequelize.INTEGER,
        field: 'commsize'
    }
});

// 시험지
const testpaper = sequelize.define('testpaper', {
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer'
    },
    tester: {
        type: Sequelize.STRING,
        field: 'tester'
    },
    startdate: {
        type: Sequelize.DATE,
        field: 'startdate'
    },
    enddate: {
        type: Sequelize.DATE,
        field: 'enddate'
    },
    grade: {
        type: Sequelize.INTEGER,
        field: 'grade'
    },
    class: {
        type: Sequelize.INTEGER,
        field: 'class'
    },
    path: {
        type: Sequelize.STRING,
        field: 'path'
    },
    total: {
        type: Sequelize.INTEGER,
        field: 'total'
    },
    question1: {
        type: Sequelize.INTEGER,
        field: 'question1'
    },
    question2: {
        type: Sequelize.INTEGER,
        field: 'question2'
    },
    question3: {
        type: Sequelize.INTEGER,
        field: 'question3'
    },
    question4: {
        type: Sequelize.INTEGER,
        field: 'question4'
    },
    question5: {
        type: Sequelize.INTEGER,
        field: 'question5'
    },
    question6: {
        type: Sequelize.INTEGER,
        field: 'question6'
    },
    question7: {
        type: Sequelize.INTEGER,
        field: 'question7'
    },
    question8: {
        type: Sequelize.INTEGER,
        field: 'question8'
    },
    question9: {
        type: Sequelize.INTEGER,
        field: 'question9'
    },
    question10: {
        type: Sequelize.INTEGER,
        field: 'question10'
    },
    question11: {
        type: Sequelize.INTEGER,
        field: 'question11'
    },
    question12: {
        type: Sequelize.INTEGER,
        field: 'question12'
    },
    question13: {
        type: Sequelize.INTEGER,
        field: 'question13'
    },
    question14: {
        type: Sequelize.INTEGER,
        field: 'question14'
    },
    question15: {
        type: Sequelize.INTEGER,
        field: 'question15'
    },
    question16: {
        type: Sequelize.INTEGER,
        field: 'question16'
    },
    question17: {
        type: Sequelize.INTEGER,
        field: 'question17'
    },
    question18: {
        type: Sequelize.INTEGER,
        field: 'question18'
    },
    question19: {
        type: Sequelize.INTEGER,
        field: 'question19'
    },
    question20: {
        type: Sequelize.INTEGER,
        field: 'question20'
    },
    question21: {
        type: Sequelize.INTEGER,
        field: 'question21'
    },
    question22: {
        type: Sequelize.INTEGER,
        field: 'question22'
    },
    question23: {
        type: Sequelize.INTEGER,
        field: 'question23'
    },
    question24: {
        type: Sequelize.INTEGER,
        field: 'question24'
    },
    question25: {
        type: Sequelize.INTEGER,
        field: 'question25'
    },
    question26: {
        type: Sequelize.INTEGER,
        field: 'question26'
    },
    question27: {
        type: Sequelize.INTEGER,
        field: 'question27'
    },
    question28: {
        type: Sequelize.INTEGER,
        field: 'question28'
    },
    question29: {
        type: Sequelize.INTEGER,
        field: 'question29'
    },
    question30: {
        type: Sequelize.INTEGER,
        field: 'question30'
    }
})

// 학생 시험지
const studentTestpaper = sequelize.define('studenttestpaper', {
    studentid: {
        type: Sequelize.STRING,
        field: 'studentid'
    },
    testpaperid: {
        type: Sequelize.INTEGER,
        field: 'testpaperid'
    },
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer'
    },
    dodate: {
        type: Sequelize.DATE,
        field: 'dodate'
    },
    grade: {
        type: Sequelize.INTEGER,
        field: 'grade'
    },
    total: {
        type: Sequelize.INTEGER,
        field: 'total'
    },
    okanser: {
        type: Sequelize.INTEGER,
        field: 'okanser'
    },
    question1anser: {
        type: Sequelize.STRING,
        field: 'question1anser'
    },
    question2anser: {
        type: Sequelize.STRING,
        field: 'question2anser'
    },
    question3anser: {
        type: Sequelize.STRING,
        field: 'question3anser'
    },
    question4anser: {
        type: Sequelize.STRING,
        field: 'question4anser'
    },
    question5anser: {
        type: Sequelize.STRING,
        field: 'question5anser'
    },
    question6anser: {
        type: Sequelize.STRING,
        field: 'question6anser'
    },
    question7anser: {
        type: Sequelize.STRING,
        field: 'question7anser'
    },
    question8anser: {
        type: Sequelize.STRING,
        field: 'question8anser'
    },
    question9anser: {
        type: Sequelize.STRING,
        field: 'question9anser'
    },
    question10anser: {
        type: Sequelize.STRING,
        field: 'question10anser'
    },
    question11anser: {
        type: Sequelize.STRING,
        field: 'question11anser'
    },
    question12anser: {
        type: Sequelize.STRING,
        field: 'question12anser'
    },
    question13anser: {
        type: Sequelize.STRING,
        field: 'question13anser'
    },
    question14anser: {
        type: Sequelize.STRING,
        field: 'question14anser'
    },
    question15anser: {
        type: Sequelize.STRING,
        field: 'question15anser'
    },
    question16anser: {
        type: Sequelize.STRING,
        field: 'question16anser'
    },
    question17anser: {
        type: Sequelize.STRING,
        field: 'question17anser'
    },
    question18anser: {
        type: Sequelize.STRING,
        field: 'question18anser'
    },
    question19anser: {
        type: Sequelize.STRING,
        field: 'question19anser'
    },
    question20anser: {
        type: Sequelize.STRING,
        field: 'question20anser'
    },
    question21anser: {
        type: Sequelize.STRING,
        field: 'question21anser'
    },
    question22anser: {
        type: Sequelize.STRING,
        field: 'question22anser'
    },
    question23anser: {
        type: Sequelize.STRING,
        field: 'question23anser'
    },
    question24anser: {
        type: Sequelize.STRING,
        field: 'question24anser'
    },
    question25anser: {
        type: Sequelize.STRING,
        field: 'question25anser'
    },
    question26anser: {
        type: Sequelize.STRING,
        field: 'question26anser'
    },
    question27anser: {
        type: Sequelize.STRING,
        field: 'question27anser'
    },
    question28anser: {
        type: Sequelize.STRING,
        field: 'question28anser'
    },
    question29anser: {
        type: Sequelize.STRING,
        field: 'question29anser'
    },
    question30anser: {
        type: Sequelize.STRING,
        field: 'question30anser'
    }
})

// 오답 기록
const wronganser = sequelize.define('wronganser', {
    userid: {
        type: Sequelize.STRING,
        field: 'userid'
    },
    questionid: {
        type: Sequelize.INTEGER,
        field: 'questionid'
    },
    questionnumber: {
        type: Sequelize.INTEGER,
        field: 'questionnumber'
    },
    wronganser: {
        type: Sequelize.STRING,
        field: 'wronganser'
    },
    testpaperid: {
        type: Sequelize.STRING,
        field: 'testpaperid'
    }
})

// 오답노트
const wrongpaper = sequelize.define('wrongpaper', {
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer'
    },
    reader: {
        type: Sequelize.STRING,
        field: 'reader'
    },
    path: {
        type: Sequelize.STRING,
        field: 'path'
    },
    commentaryPath: {
        type: Sequelize.STRING,
        field: 'commentaryPath'
    },
    total: {
        type: Sequelize.INTEGER,
        field: 'total'
    },
    question1: {
        type: Sequelize.INTEGER,
        field: 'question1'
    },
    question2: {
        type: Sequelize.INTEGER,
        field: 'question2'
    },
    question3: {
        type: Sequelize.INTEGER,
        field: 'question3'
    },
    question4: {
        type: Sequelize.INTEGER,
        field: 'question4'
    },
    question5: {
        type: Sequelize.INTEGER,
        field: 'question5'
    },
    question6: {
        type: Sequelize.INTEGER,
        field: 'question6'
    },
    question7: {
        type: Sequelize.INTEGER,
        field: 'question7'
    },
    question8: {
        type: Sequelize.INTEGER,
        field: 'question8'
    },
    question9: {
        type: Sequelize.INTEGER,
        field: 'question9'
    },
    question10: {
        type: Sequelize.INTEGER,
        field: 'question10'
    },
    question11: {
        type: Sequelize.INTEGER,
        field: 'question11'
    },
    question12: {
        type: Sequelize.INTEGER,
        field: 'question12'
    },
    question13: {
        type: Sequelize.INTEGER,
        field: 'question13'
    },
    question14: {
        type: Sequelize.INTEGER,
        field: 'question14'
    },
    question15: {
        type: Sequelize.INTEGER,
        field: 'question15'
    },
    question16: {
        type: Sequelize.INTEGER,
        field: 'question16'
    },
    question17: {
        type: Sequelize.INTEGER,
        field: 'question17'
    },
    question18: {
        type: Sequelize.INTEGER,
        field: 'question18'
    },
    question19: {
        type: Sequelize.INTEGER,
        field: 'question19'
    },
    question20: {
        type: Sequelize.INTEGER,
        field: 'question20'
    },
    question21: {
        type: Sequelize.INTEGER,
        field: 'question21'
    },
    question22: {
        type: Sequelize.INTEGER,
        field: 'question22'
    },
    question23: {
        type: Sequelize.INTEGER,
        field: 'question23'
    },
    question24: {
        type: Sequelize.INTEGER,
        field: 'question24'
    },
    question25: {
        type: Sequelize.INTEGER,
        field: 'question25'
    },
    question26: {
        type: Sequelize.INTEGER,
        field: 'question26'
    },
    question27: {
        type: Sequelize.INTEGER,
        field: 'question27'
    },
    question28: {
        type: Sequelize.INTEGER,
        field: 'question28'
    },
    question29: {
        type: Sequelize.INTEGER,
        field: 'question29'
    },
    question30: {
        type: Sequelize.INTEGER,
        field: 'question30'
    }
})

// 리포트 기록
const report = sequelize.define('report', {
    userid: {
        type: Sequelize.STRING,
        field: 'userid'
    },
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    path: {
        type: Sequelize.STRING,
        field: 'path'
    },
    commentaryPath: {
        type: Sequelize.STRING,
        field: 'commentaryPath'
    },    
    question1: {
        type: Sequelize.INTEGER,
        field: 'question1'
    },
    question2: {
        type: Sequelize.INTEGER,
        field: 'question2'
    },
    question3: {
        type: Sequelize.INTEGER,
        field: 'question3'
    },
    question4: {
        type: Sequelize.INTEGER,
        field: 'question4'
    },
    question5: {
        type: Sequelize.INTEGER,
        field: 'question5'
    },
    question6: {
        type: Sequelize.INTEGER,
        field: 'question6'
    },
    question7: {
        type: Sequelize.INTEGER,
        field: 'question7'
    },
    question8: {
        type: Sequelize.INTEGER,
        field: 'question8'
    },
    question9: {
        type: Sequelize.INTEGER,
        field: 'question9'
    },
    question10: {
        type: Sequelize.INTEGER,
        field: 'question10'
    },
    question11: {
        type: Sequelize.INTEGER,
        field: 'question11'
    },
    question12: {
        type: Sequelize.INTEGER,
        field: 'question12'
    },
    question13: {
        type: Sequelize.INTEGER,
        field: 'question13'
    },
    question14: {
        type: Sequelize.INTEGER,
        field: 'question14'
    },
    question15: {
        type: Sequelize.INTEGER,
        field: 'question15'
    },
    question16: {
        type: Sequelize.INTEGER,
        field: 'question16'
    },
    question17: {
        type: Sequelize.INTEGER,
        field: 'question17'
    },
    question18: {
        type: Sequelize.INTEGER,
        field: 'question18'
    },
    question19: {
        type: Sequelize.INTEGER,
        field: 'question19'
    },
    question20: {
        type: Sequelize.INTEGER,
        field: 'question20'
    },
    question21: {
        type: Sequelize.INTEGER,
        field: 'question21'
    },
    question22: {
        type: Sequelize.INTEGER,
        field: 'question22'
    },
    question23: {
        type: Sequelize.INTEGER,
        field: 'question23'
    },
    question24: {
        type: Sequelize.INTEGER,
        field: 'question24'
    },
    question25: {
        type: Sequelize.INTEGER,
        field: 'question25'
    },
    question26: {
        type: Sequelize.INTEGER,
        field: 'question26'
    },
    question27: {
        type: Sequelize.INTEGER,
        field: 'question27'
    },
    question28: {
        type: Sequelize.INTEGER,
        field: 'question28'
    },
    question29: {
        type: Sequelize.INTEGER,
        field: 'question29'
    },
    question30: {
        type: Sequelize.INTEGER,
        field: 'question30'
    }
})

// 로그인 기록
const loginhistory = sequelize.define('loginhistory', {
    userid: {
        type: Sequelize.STRING,
        field: 'userid'
    },
    ip: {
        type: Sequelize.STRING,
        field: 'ip'
    },
    memo: {
        type: Sequelize.STRING,
        field: 'memo'
    }
})

// 교재명 코드
const textbookCode = sequelize.define('textbookcode', {
    nGrade: {
        type: Sequelize.INTEGER,
        field: 'nGrade'
    },
    sCode: {
        type: Sequelize.STRING,
        field: 'sCode'
    },
    sValue: {
        type: Sequelize.STRING,
        field: 'sValue'
    },
    mine: {
        type: Sequelize.STRING,
        field: 'mine'
    }
})

// 단원 코드
const unitCode = sequelize.define('unitCode', {
    nGrade: {
        type: Sequelize.INTEGER,
        field: 'nGrade'
    },
    sCode: {
        type: Sequelize.STRING,
        field: 'sCode'
    },
    sValue: {
        type: Sequelize.STRING,
        field: 'sValue'
    }
})

// 유형 코드
const typeCode = sequelize.define('typeCode', {
    nGrade: {
        type: Sequelize.INTEGER,
        field: 'nGrade'
    },
    sCode: {
        type: Sequelize.STRING,
        field: 'sCode'
    },
    sValue: {
        type: Sequelize.STRING,
        field: 'sValue'
    }
})

module.exports = {
    sequelize: sequelize,
    User: User,
    StudentClass: StudentClass,
    notiBoard: notiBoard,
    notiReply: notiReply,
    textbook: textbook,
    question: question,
    testpaper: testpaper,
    studentTestpaper: studentTestpaper,
    loginhistory: loginhistory,
    wronganser: wronganser,
    report: report,
    textbookCode: textbookCode,
    unitCode: unitCode,
    typeCode: typeCode,
    wrongpaper: wrongpaper,
    consulting: consulting,
    consultingReply: consultingReply,
};